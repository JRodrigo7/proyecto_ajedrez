create database Club_Ajedrez2;
use Club_Ajedrez2;
drop database Club_Ajedrez2;
#REGION TABLAS 
show tables;


#region paises
drop table paises;
drop table paises;
create table paises(idpais int AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(50), numeroclub int, paisrepresentante varchar(50));

create view v_paises as
select idpais as 'id', nombre as 'pais', numeroclub as 'clubes', Paisrepresentante 
from paises;
select * from v_paises;
select * from paises;

#region procedures paises
/*insertar*/
drop procedure  if exists p_insertarPaises;
create procedure p_insertarPaises(in pid int, in pnombre varchar(50), in pnumclubs int, in pfkpaisrepresentante varchar(50))
begin 
insert into paises values(null, pnombre , pnumclubs, pfkpaisrepresentante);
end;
/*actualizar*/
drop procedure if exists actualizar_paises;
create procedure actualizar_paises(in pnombre varchar(50), in pnumclubs int, in pfkpaisrepresentante int, in pid varchar(50))
begin
update paises set nombre = pnombre, numeroclub = pnumclubs, paisrepresentante = pfkpaisrepresentante where idpais = pid;
end;
/*borrar*/
create procedure eliminar_paises(in pid int)
begin
delete from paises where idpais = pid;
end;

call  eliminar_paises(5);
call actualizar_paises('ALEMANIA',10,4,3);
select * from paises;
call p_insertarPaises(6,'Antigua y Barbuda', 10,6);





#region paises insertados
call p_insertarPaises(null,' Afganist�n', null,null); 
call p_insertarPaises(null,' Albania',null,null);
call p_insertarPaises(null,' Alemania',null,null);
call p_insertarPaises(null,' Andorra', null,null);
call p_insertarPaises(null,'Angola',null,null);
call p_insertarPaises(null,' Antigua y Barbuda',null,null);
call p_insertarPaises(null,' Arabia Saudita',null,null);
call p_insertarPaises(null,' Argelia', null,null);
call p_insertarPaises(null,'Argentina', null,null);
call p_insertarPaises(null,'Armenia', null,null);
call p_insertarPaises(null,'Australia', null,null);
call p_insertarPaises(null,'Austria', null,null);
call p_insertarPaises(null,' Azerbaiy�n', null,null);
call p_insertarPaises(null,' Bahamas', null,null);
call p_insertarPaises(null,' Banglad�s', null,null);
call p_insertarPaises(null,' Barbados', null,null);
call p_insertarPaises(null,' Bar�in', null,null);
call p_insertarPaises(null,' B�lgica', null,null);
call p_insertarPaises(null,' Belice', null,null);
call p_insertarPaises(null,' Ben�n', null,null);
call p_insertarPaises(null,' Bielorrusia', null,null);
call p_insertarPaises(null,' Birmania Myanmar', null,null);
call p_insertarPaises(null,' Bolivia', null,null);
call p_insertarPaises(null,' Bosnia y Herzegovina', null,null);
call p_insertarPaises(null,' Botsuana', null,null);
call p_insertarPaises(null,' Brasil', null,null);
call p_insertarPaises(null,' Brun�i', null,null);
call p_insertarPaises(null,' Bulgaria', null,null);
call p_insertarPaises(null,' Burkina Faso', null,null);
call p_insertarPaises(null,' Burundi', null,null);
call p_insertarPaises(null,' But�n', null,null);
call p_insertarPaises(null,' Cabo Verde', null,null);
call p_insertarPaises(null,' Camboya', null,null);
call p_insertarPaises(null,' Camer�n', null,null);
call p_insertarPaises(null,' Canad�', null,null);
call p_insertarPaises(null,' Catar', null,null);
call p_insertarPaises(null,' Chad', null,null);
call p_insertarPaises(null,' Chile', null,null);
call p_insertarPaises(null,' China', null,null);
call p_insertarPaises(null,' Chipre', null,null);
call p_insertarPaises(null,' Ciudad del Vaticano', null,null);
call p_insertarPaises(null,' Colombia', null,null);
call p_insertarPaises(null,' Comoras', null,null);
call p_insertarPaises(null,' Corea del Norte', null,null);
call p_insertarPaises(null,' Corea del Sur', null,null);
call p_insertarPaises(null,' Costa de Marfil', null,null);
call p_insertarPaises(null,' Costa Rica', null,null);
call p_insertarPaises(null,' Croacia', null,null);
call p_insertarPaises(null,' Cuba', null,null);
call p_insertarPaises(null,' Dinamarca', null,null);
call p_insertarPaises(null,' Dominica', null,null);
call p_insertarPaises(null,' Ecuador', null,null);
call p_insertarPaises(null,' Egipto', null,null);
call p_insertarPaises(null,' El Salvador', null,null);
call p_insertarPaises(null,' Emiratos �rabes Unidos', null,null);
call p_insertarPaises(null,' Eritrea', null,null);
call p_insertarPaises(null,' Eslovaquia', null,null); 
call p_insertarPaises(null,' Eslovenia', null,null);
call p_insertarPaises(null,' Espa�a', null,null);
call p_insertarPaises(null,' Estados Unidos', null,null);
call p_insertarPaises(null,' Estonia', null,null);
call p_insertarPaises(null,' Etiop�a ', null,null);
call p_insertarPaises(null,' Filipinas ', null,null);
call p_insertarPaises(null,' Finlandia ', null,null);
call p_insertarPaises(null,' Fiyi ', null ,null);
call p_insertarPaises(null,' Francia ', null,null);
call p_insertarPaises(null,' Gab�n ', null,null);
call p_insertarPaises(null,' Gambia ', null,null);
call p_insertarPaises(null,' Georgia ', null,null);
call p_insertarPaises(null,' Ghana ', null,null);
call p_insertarPaises(null,' Granada', null,null);
call p_insertarPaises(null,' Grecia ', null,null);
call p_insertarPaises(null,' Guatemala ', null,null);
call p_insertarPaises(null,' Guyana ', null,null);
call p_insertarPaises(null,' Guinea ', null,null);
call p_insertarPaises(null,' Guinea ecuatorial ', null,null);
call p_insertarPaises(null,' Guinea-Bis�u', null,null);
call p_insertarPaises(null,' Hait� ', null,null);
call p_insertarPaises(null,' Honduras ', null,null);
call p_insertarPaises(null,' Hungr�a ', null,null);
call p_insertarPaises(null,' India ', null,null);
call p_insertarPaises(null,' Indonesia ', null,null);
call p_insertarPaises(null,' Irak', null,null);
call p_insertarPaises(null,' Ir�n ', null,null);
call p_insertarPaises(null,' Irlanda ', null,null);
call p_insertarPaises(null,' Islandia ', null,null);
call p_insertarPaises(null,' Islas Marshall', null,null);
call p_insertarPaises(null,' Islas Salom�n',null,null);
call p_insertarPaises(null,' Israel ',null,null);
call p_insertarPaises(null,' Italia',null,null);
call p_insertarPaises(null,' Jamaica',null,null);
call p_insertarPaises(null,' Jap�n',null,null);
call p_insertarPaises(null,' Jordania',null,null);
call p_insertarPaises(null,' Kazajist�n',null,null);
call p_insertarPaises(null,' Kenia',null,null);
call p_insertarPaises(null,' Kirguist�n',null,null);
call p_insertarPaises(null,' Kiribati',null,null);
call p_insertarPaises(null,' Kuwait',null,null);
call p_insertarPaises(null,' Laos',null,null);
call p_insertarPaises(null,' Lesoto',null,null);
call p_insertarPaises(null,' Letonia',null,null);
call p_insertarPaises(null,' L�bano',null,null);
call p_insertarPaises(null,' Liberia',null,null);
call p_insertarPaises(null,' Libia',null,null);
call p_insertarPaises(null,' Liechtenstein',null,null);
call p_insertarPaises(null,' Lituania',null,null);
call p_insertarPaises(null,' Luxemburgo',null,null);
call p_insertarPaises(null,' Macedonia del Norte',null,null);
call p_insertarPaises(null,' Madagascar',null,null);
call p_insertarPaises(null,' Malasia',null,null);
call p_insertarPaises(null,' Malaui',null,null);
call p_insertarPaises(null,' Maldivas',null,null);
call p_insertarPaises(null,' Mal�',null,null);
call p_insertarPaises(null,' Malta',null,null);
call p_insertarPaises(null,' Marruecos',null,null);
call p_insertarPaises(null,' Mauricio',null,null);
call p_insertarPaises(null,' Mauritania',null,null);
call p_insertarPaises(null,' M�xico',null,null);
call p_insertarPaises(null,' Micronesia',null,null);
call p_insertarPaises(null,' Moldavia',null,null);
call p_insertarPaises(null,' M�naco',null,null);
call p_insertarPaises(null,' Mongolia',null,null);
call p_insertarPaises(null,' Montenegro',null,null);
call p_insertarPaises(null,' Mozambique',null,null);
call p_insertarPaises(null,' Namibia',null,null);
call p_insertarPaises(null,' Nauru',null,null);
call p_insertarPaises(null,' Nepal',null,null);
call p_insertarPaises(null,' Nicaragua',null,null);
call p_insertarPaises(null,' N�ger',null,null);
call p_insertarPaises(null,' Nigeria',null,null);
call p_insertarPaises(null,' Noruega',null,null);
call p_insertarPaises(null,' Nueva Zelanda',null,null);
call p_insertarPaises(null,' Om�n',null,null);
call p_insertarPaises(null,' Pa�ses Bajos', null,null);
call p_insertarPaises(null,' Pakist�n', null,null);
call p_insertarPaises(null,' Palaos ', null,null);
call p_insertarPaises(null,' Panam�', null,null);
call p_insertarPaises(null,' Pap�a Nueva Guinea', null,null);
call p_insertarPaises(null,' Paraguay ', null,null);
call p_insertarPaises(null,' Per� ', null,null);
call p_insertarPaises(null,' Polonia', null,null);
call p_insertarPaises(null,' Portugal', null,null);
call p_insertarPaises(null,' Reino Unido', null,null);
call p_insertarPaises(null,' Rep�blica Centroafricana ', null,null);
call p_insertarPaises(null,' Rep�blica Checa ', null,null);
call p_insertarPaises(null,' Rep�blica del Congo ', null,null);
call p_insertarPaises(null,' Rep�blica Democr�tica del Congo', null,null);
call p_insertarPaises(null,' Rep�blica Dominicana', null,null);
call p_insertarPaises(null,' Rep�blica Sudafricana ', null,null);
call p_insertarPaises(null,' Ruanda ', null,null);
call p_insertarPaises(null,' Ruman�a ', null,null);
call p_insertarPaises(null,' Rusia ', null,null);
call p_insertarPaises(null,' Samoa ', null,null);
call p_insertarPaises(null,' Santa Luc�a ', null,null);
call p_insertarPaises(null,' Santo Tom� y Pr�ncipe ', null,null);
call p_insertarPaises(null,' San Vicente y las Granadinas ', null,null);
call p_insertarPaises(null,' San Marino ', null,null);
call p_insertarPaises(null,' San Crist�bal y Nieves ', null,null);
call p_insertarPaises(null,' Senegal ', null,null);
call p_insertarPaises(null,' Serbia ', null,null);
call p_insertarPaises(null,' Seychelles', null,null);
call p_insertarPaises(null,' Sierra Leona ', null,null);
call p_insertarPaises(null,' Singapur ', null,null);
call p_insertarPaises(null,' Siria', null,null);
call p_insertarPaises(null,' Somalia', null,null);
call p_insertarPaises(null,' Sri Lanka ', null,null);
call p_insertarPaises(null,' Suazilandia ', null,null);
call p_insertarPaises(null,' Sud�n', null,null);
call p_insertarPaises(null,' Sud�n del Sur ', null,null);
call p_insertarPaises(null,' Suecia ', null,null);
call p_insertarPaises(null,' Suiza ', null,null);
call p_insertarPaises(null,' Surinam ', null,null);
call p_insertarPaises(null,' Tanzania ', null,null);
call p_insertarPaises(null,' Tailandia ', null,null);
call p_insertarPaises(null,' Tanzania ', null,null);
call p_insertarPaises(null,' Tayikist�n ', null,null);
call p_insertarPaises(null,' Timor Oriental ', null,null);
call p_insertarPaises(null,' Togo ', null,null);
call p_insertarPaises(null,' Tonga ', null,null);
call p_insertarPaises(null,' Trinidad y Tobago ', null,null);
call p_insertarPaises(null,' T�nez ', null,null);
call p_insertarPaises(null,' Turkmenist�n ', null,null);
call p_insertarPaises(null,' Turqu�a ', null,null);
call p_insertarPaises(null,' Tuvalu', null,null);
call p_insertarPaises(null,' Ucrania', null,null);
call p_insertarPaises(null,' Uganda ', null,null);
call p_insertarPaises(null,' Uruguay ', null,null);
call p_insertarPaises(null,' Uzbekist�n ', null,null);
call p_insertarPaises(null,' Vanuatu ', null,null);
call p_insertarPaises(null,' Venezuela ', null,null);
call p_insertarPaises(null,' Vietnam ', null,null);
call p_insertarPaises(null,' Yemen ', null,null);
call p_insertarPaises(null,' Yibuti ', null,null);
call p_insertarPaises(null,' Zambia ', null,null);
call p_insertarPaises(null,' Zimbabue', null,null);

select * from paises;
delete from paises where idpais = 15;
#end region

#end region 
#end region



#region participante
drop table participantes;
create table participantes(idparticipante int auto_increment PRIMARY KEY, numsocio varchar(10), nombre varchar(100), 
apellidop varchar(100), apellidom varchar(100), direccion varchar(150), telefono varchar(10), paisquerepresenta int, 
foreign key(paisquerepresenta) references paises(idpais) on delete cascade on update cascade);

#region VISTA PARTICIPANTE

create view v_participantes as

select idparticipante, numsocio as 'Socio', participantes.nombre as 'Nombre', apellidop as 'Ap.Paterno',
apellidom as 'Ap.Materno',direccion as 'Direccion', telefono as 'Telefono', paises.nombre as 'Pais' from participantes,paises
where participantes.paisquerepresenta = paises.idpais; 

select * from v_participantes;


#end region



#end region



#region jugadores
drop table jugadores;
create table jugadores(idjugador int primary key, nivel int,
foreign key (idjugador) references participantes(idparticipante)on delete cascade on update cascade);

#region VISTA JUGADORES

create view v_jugadores as

select idjugador, numsocio as 'Socio', participantes.nombre as 'Nombre', apellidop as 'Ap.Paterno',
apellidom as 'Ap.Materno',direccion as 'Direccion', telefono as 'Telefono',nivel as 'Nivel', paises.nombre as 'Pais' 
from jugadores,participantes,paises
where participantes.paisquerepresenta = paises.idpais and idjugador = idparticipante;

select * from v_jugadores;

#end region

#region Vista Combo
create view v_jugadores3 as

select idjugador, numsocio as 'Socio',concat(participantes.nombre," ",apellidop," ",apellidom) 
as 'Nombre',direccion as 'Direccion', telefono as 'Telefono',nivel as 'Nivel', paises.nombre as 'Pais' 
from jugadores,participantes,paises
where participantes.paisquerepresenta = paises.idpais and idjugador = idparticipante;

select * from v_jugadores3;

#end region

#region procedures jugadores
#region PROCEDURE INSERTAR  JUGADOR/PARTICIPANTE
drop procedure if exists p_inJugadorP;
create procedure p_inJugadorP(
in _nsP varchar(10),
in _nmP varchar(100),
in _appP varchar(100),
in _apmP varchar(100),
in _dirP varchar(150),
in _telP varchar(10),
in _fkp int,

in _nivel int)


begin 
declare w int;
declare x int;
declare y int;
declare z int;

declare _idR int;
declare incremento int;
/* select count(*) from alumno
 where fkpersona =_fkP into a;*/
 select count(*) from participantes
 where nombre=_nmP into w;
  select count(*) from participantes
 where apellidop=_appP into x;
  select count(*) from participantes
 where apellidom=_apmP into y;
  
 set z = (w+x+y);
 if z<=2 then
 select Max(idparticipante) from participantes into incremento;
 if incremento is null then
 set _idR = 1;
 end if;
 if incremento >= 1 then
 set _idR = incremento + 1;
 end if;
 
insert into participantes values(_idR,_nsP,_nmP,_appP,_apmP,_dirP,_telP,_fkp);
insert into jugadores values(_idR,_nivel);
select "Registro exitoso";
end if;
if z>=3 then
select "Este participante ya ha sido registrado";
end if;
end;

call p_inJugadorP('2825','Rick','Lo','Abdu','River red #25','897452145',12,1);
call p_inJugadorP('2525','Jose','Morelos','Aasas','River red #25','897452145',14,1);
select * from participantes;
select * from jugadores;
select * from paises;
#end region

#region PROCEDURE ACTUALIZAR JUGADOR/PARTICIPANTE
drop procedure if exists p_actualizarJugadorP;

create procedure p_actualizarJugadorP(
in pnumsociado varchar(10), 
in pnombre varchar(100),
in papellidop varchar(100),
in papellidom varchar(100),
in pdireccion varchar(150),
in ptelefono varchar(10), 
in ppais int,
in idj int, 
in nvl int)
begin
update participantes set numsocio = pnumsociado, nombre = pnombre, apellidop = papellidop, apellidom = papellidom,
direccion = pdireccion, telefono = ptelefono, paisquerepresenta = ppais where idparticipante = idj;
update jugadores set nivel = nvl where idjugador = idj;
select "Jugador actualizado";
end;

call p_actualizarJugadorP('123','axel','prez','anda','sanmiguel','22222',13,1,1);
select * from participantes;
select * from jugadores;


#end region

#region BORRAR JUGADOR/PARTICIPANTE
drop procedure if exists p_delJugadorP;

create procedure p_delJugadorP(in pid int)
begin
delete from jugadores where idjugador = pid;
delete from participantes where idparticipante = pid;
end;
call p_delJugadorP(1);
select * from participantes;

#end region

#end region

#end region


#region arbitros
create table arbitros(idarbitro int primary key, 
foreign key(idarbitro) references participantes(idparticipante)on delete cascade on update cascade);

#region VISTA ARBITROS

create view v_arbitros as

select idarbitro, numsocio as 'Socio', participantes.nombre as 'Nombre', apellidop as 'Ap.Paterno',
apellidom as 'Ap.Materno',direccion as 'Direccion', telefono as 'Telefono', paises.nombre as 'Pais' 
from arbitros,participantes,paises
where participantes.paisquerepresenta = paises.idpais and idarbitro = idparticipante;

select * from v_arbitros;

#end region

#region VISTA ARBITROS combo

create view v_arbitros2 as

select idarbitro, numsocio as 'Socio',concat(participantes.nombre," ", apellidop," ",
apellidom) as 'Nombre',direccion as 'Direccion', telefono as 'Telefono', paises.nombre as 'Pais' 
from arbitros,participantes,paises
where participantes.paisquerepresenta = paises.idpais and idarbitro = idparticipante;

select * from v_arbitros2;

#end region

#region INSERTAR ARBITRO/PARTICIPANTE

drop procedure if exists p_inArbitroP;
create procedure p_inArbitroP(
in _nsP varchar(10),
in _nmP varchar(100),
in _appP varchar(100),
in _apmP varchar(100),
in _dirP varchar(150),
in _telP varchar(10),
in _fkp int)


begin 
declare w int;
declare x int;
declare y int;
declare z int;

declare _idR int;
declare incremento int;

 select count(*) from participantes
 where nombre=_nmP into w;
  select count(*) from participantes
 where apellidop=_appP into x;
  select count(*) from participantes
 where apellidom=_apmP into y;
  
 set z = (w+x+y);
 if z<=2 then
 select Max(idparticipante) from participantes into incremento;
 if incremento is null then
 set _idR = 1;
 end if;
 if incremento >= 1 then
 set _idR = incremento + 1;
 end if;
 
insert into participantes values(_idR,_nsP,_nmP,_appP,_apmP,_dirP,_telP,_fkp);
insert into arbitros values(_idR);
select "Registro exitoso";
end if;
if z>=3 then
select "Este participante ya ha sido registrado";
end if;
end;

call p_inArbitroP('0808','Rick','Lo','Abdu','River red #25','897452145',1);
call p_inArbitroP('0805','Luis','lopez','Garcia','River red #25','897452145',15);
select * from participantes;
select * from arbitros;

#end region

#region ACTUALIZAR ARBITRO/PARTICIPANTE
drop procedure if exists p_actualizarArbitroP;
create procedure p_actualizarArbitroP(
in pnumsociado varchar(10), 
in pnombre varchar(100),
in papellidop varchar(100),
in papellidom varchar(100),
in pdireccion varchar(150),
in ptelefono varchar(10), 
in ppais int,
in ida int)
begin
update participantes set numsocio = pnumsociado, nombre = pnombre, apellidop = papellidop, apellidom = papellidom,
direccion = pdireccion, telefono = ptelefono, paisquerepresenta = ppais where idparticipante = ida;
select "Datos de Arbitro actualizados";

end;

call p_actualizarArbitroP('123','axel','prez','anda','sanmiguel','22222',1,2);
select * from participantes;
select * from jugadores;

#end region

#region ELIMINAR ARBITRO/PARTICIPANTE

drop procedure if exists p_delJugadorP;

create procedure p_delArbitroP(in pid int)
begin
delete from arbitros where idarbitro = pid;
delete from participantes where idparticipante = pid;
end;

call p_delArbitroP(4);

select * from participantes;

#end region


#end region 



#region hoteles
create table hoteles(idhotel int primary key auto_increment, nombre varchar(100), direccion varchar(100), 
telefono varchar(100));

select * from hoteles where nombre like 'Monterreal';
#region procedures hoteles
/*insertar*/
create procedure insertar_hoteles(in pid int, in pnombre varchar(50), in pdireccion varchar(50), in ptelefono varchar(10))
begin 
insert into hoteles values(null, pnombre , pdireccion, ptelefono);
end;
call insertar_hoteles(null,'weqe','qweqe','1234');
select * from hoteles;
/*actualizar*/

create procedure actualizar_hoteles(in pnombre varchar(50), in pdireccion varchar(50), in ptelefono varchar(10), in pid int)
begin
update hoteles set nombre = pnombre, direccion = pdireccion, telefono = ptelefono where idhotel = pid;
end;
call actualizar_hoteles('Casa Grande','laferia','123453',1);
/*eliminar*/

create procedure eliminar_hoteles(in pid int)
begin
delete from hoteles where idhotel = pid;
end;
call eliminar_hoteles(1);
#end region 
#end region



#region salas
create table salas(idsala int primary key auto_increment, numsala int, capacidad int, medios varchar(200), fkhotel int,
foreign key(fkhotel) references hoteles(idhotel)on delete cascade on update cascade);

#region Vista Salas

create view v_sala as
select idsala,numsala,capacidad,medios,nombre as 'Hotel' 
from salas,hoteles
where salas.fkhotel = hoteles.idhotel;
select * from v_sala;

Select * from v_sala where Hotel like "Monterreal";

#end region

#region procedures salas

/*insertar*/
drop procedure insertar_salas;
create procedure insertar_salas(in pid int,in pnumerosala int, in pcapacidad int, in pmedios varchar(100),in pfkhotel int)
begin 
insert into salas values(null, pnumerosala , pcapacidad, pmedios, pfkhotel);
end;
call insertar_salas(null,12,20,'qweqwe',1);
select * from hoteles;
select * from salas;
/*actualizar*/
drop procedure actualizar_salas;
create procedure actualizar_salas(in pnumerosala int, in pcapacidad int, in pmedios varchar(200), in pfkhotel int, in pid int)
begin
update salas set numsala = pnumerosala, capacidad = pcapacidad, medios = pmedios, fkhotel = pfkhotel where idsala = pid;
end;

call actualizar_salas(12,20,'qweqweQWQWE',2,4);
select idhotel from hoteles;
/*eliminar*/
create procedure eliminar_salas(in pid int)
begin
delete from salas where idsala = pid;
end;

call eliminar_salas(3);
#end region
#end region



#region hospedajes
create table hospedajes(idhospedaje int auto_increment primary key, 
fkparticipante int, 
fkhotel int, 
fechainicial varchar(50),
fechafinal varchar(50), 
foreign key(fkparticipante) references participantes(idparticipante),
foreign key(fkhotel) references hoteles(idhotel)on delete cascade on update cascade);

 

#region Vista Hospedajes
create view v_hospedaje as
select hospedajes.idhospedaje,concat(participantes.apellidop,' ',participantes.apellidom) as 'Apellidos',
participantes.nombre as 'Participante',
hoteles.nombre,hospedajes.fechainicial,hospedajes.fechafinal 
from hospedajes,participantes,hoteles
where hospedajes.fkparticipante = participantes.idparticipante and hospedajes.fkhotel = hoteles.idhotel;

select * from v_hospedaje;

#end region


#region procedures hospedajes

/*insertar*/

create procedure insertar_hospedajes(in pid int, in pfkparticipante int, in pfkhotel int, in pfechainicial varchar(50),
in pfechafinal varchar(50))
begin
insert into hospedajes values(null, pfkparticipante, pfkhotel, pfechainicial, pfechafinal);
end;


select * from participantes;
select * from hoteles;
call insertar_hospedajes(null,1,1,'131 de abril del 2020', '123 de mayo del 2020');
call insertar_hospedajes(null,1,2,'20 de abril del 2020',' 20 de mayo del 2020');
select * from hospedajes;

select * from participantes;

/*actualizar*/

create procedure actualizar_hospedajes(in pfkparticipante int, in pfkhotel int, in pfechainicial varchar(50),
in pfechafinal varchar(50),in pid int)
begin
update hospedajes set fkparticipante= pfkparticipante, fkhotel= pfkhotel, fechainicial =  pfechainicial , 
fechafinal =  pfechafinal where idhospedaje = pid;
end;



call actualizar_hospedajes(1,2,'13 de febrero del 2020', '12 de mayo del 2020',1);
select * from hospedajes;

/*borrar*/


create procedure eliminar_hospedaje(in pid int)
begin
delete from hospedajes where idhospedaje = pid;
end;

call eliminar_hospedaje(2);
select * from hospedajes;

#end region 

#end region



#region partidas

/*create table partidas(idpartida int auto_increment primary key, jugadorb int, jugadorn int, arbitro int, sala int,
fecha varchar(50), foreign key(jugadorb) references jugadores(idjugador), foreign key(jugadorn) references jugadores(idjugador),
foreign key(arbitro) references arbitros(idarbitro), foreign key(sala) references salas(idsala)on delete cascade on update cascade);*/

/*NUEVA TABLA PARTIDA */
create table partidas(idpartida int auto_increment primary key, jugadorb int, jugadorn int, arbitro int, sala int,
fecha varchar(50),jornada varchar(20), foreign key(jugadorb) references jugadores(idjugador), foreign key(jugadorn) references jugadores(idjugador),
foreign key(arbitro) references arbitros(idarbitro), foreign key(sala) references salas(idsala)on delete cascade on update cascade);



#region VISTA PARTIDA
select * from partidas;
select * from v_partidas;
drop view v_partidas;

create view v_partidas as
SELECT P.idpartida,concat(J1.nombre,' ',J1.apellidop,' ',J1.apellidom) as 'Jugador_Blancas', 
concat(J2.nombre,' ',J2.apellidop,' ',J2.apellidom) as 'Jugador_Negras',
concat(A.nombre,' ',A.apellidop,' ',A.apellidom) as 'Arbitro',
P.sala as 'Sala',P.fecha as 'Fecha',P.jornada as 'Jornada'
FROM partidas P
INNER JOIN participantes J1 ON J1.idparticipante = P.jugadorb
INNER JOIN participantes J2 ON J2.idparticipante = P.jugadorn
INNER JOIN participantes A ON A.idparticipante = P.arbitro
ORDER BY P.idpartida;

call actualizar_partidas(6, 7, 8, 1, '01-06-2020', '2020-06',5);
#end region

#region procedures partidas 
/*insertar*/

/*create procedure insertar_partidas(in pidpartida int,in pjugadorb int, in pjugadorn int, in pfkarbitro int, in pfksala int, in pfecha varchar(50))
begin
insert into partidas values(null,pjugadorb, pjugadorn, pfkarbitro, pfksala, pfecha);
end;

call insertar_partidas(null,1,1,1,4,'12 de mayo del 2020');
select * from jugadores;
select * from arbitros;
select * from salas;
select * from partidas;*/

/*NUEVO PROCEDURE*/
drop procedure if exists p_inPartida;
create procedure p_inPartida(
in pjugadorb int, 
in pjugadorn int, 
in parbitro int, 
in psala int, 
in pfecha varchar(50),
in pjornada varchar(20))

begin 
declare w int;
declare x int;
declare y int;

select paisquerepresenta from participantes
 where idparticipante = parbitro into w;
  select paisquerepresenta from participantes
 where idparticipante = pjugadorb into x;
  select paisquerepresenta from participantes
 where idparticipante = pjugadorn into y;

 if w != x and w !=y and x !=y then

insert into partidas values(null,pjugadorb,pjugadorn,parbitro,psala,pfecha,pjornada);
select "Partida Registrada";
end if;
if w=x or w=y or x=y then
select "El arbitro no puede pertenecer al mismo pa�s que los jugadores";
end if;
end;

call p_inPartida(1,4,3,1,'25-02-21',202102);

call p_inPartida(3, 6, 8, 4, '30-01-2020', '202001');






/*actualizar*/

create procedure actualizar_partidas(
in pjugadorb int, 
in pjugadorn int, 
in parbitro int, 
in psala int, 
in pfecha varchar(50),
in pjornada varchar(20),
in pid int)
begin
update partidas set jugadorb = pjugadorb, jugadorn = pjugadorn, arbitro = parbitro, sala = psala, fecha = pfecha, jornada = pjornada where idpartida = pid ;
end;
call actualizar_partidas();

call actualizar_partidas(6,7,8,1,'01-06-2020','2020-06',5);
select * from partidas;


/*eliminar*/

create procedure eliminar_partidas(in pid int)
begin
delete from partidas where idpartida = pid;
end;

call eliminar_partidas(2);

#end region

#end region



#region movimientos
create table movimientos(idmovimiento int auto_increment primary key, nummovimiento int, posiciones varchar(50),
descripcion varchar(100), fkpartida int, foreign key (fkpartida) references partidas(idpartida)on delete cascade on update cascade);

#region procedures movimientos 
/*insertar*/

create procedure insertar_movimientos(in pid int, in pnummovimiento int, in pposiciones varchar(50), in pdescripcion varchar(100),
in pfkpartida int)
begin 
insert into movimientos values(null, pnummovimiento,  pposiciones, pdescripcion,  pfkpartida);
end;

call insertar_movimientos(null,3,'delanteros','alto',1);
select * from partidas;
select * from movimientos;

/*actualizar*/

create procedure actualizar_movimientos(in pnummovimiento int, in pposiciones varchar(20), in pdescripcion  varchar(50),
in pfkpartida int, in pid int)
begin
update movimientos set nummovimiento = pnummovimiento, posiciones = pposiciones, descripcion = pdescripcion, 
fkpartida = pfkpartida where idmovimiento = pid;
end;

call actualizar_movimientos(3,'delanteros','MUY ALTO',1,1);
select * from movimientos;
/*eliminar*/

create procedure eliminar_movimientos(in pid int)
begin
delete from movimientos where idmovimiento = pid;
end;
call eliminar_movimientos(2);


#end region 
#end region

#region Usuarios
drop table admin;
create table admin(id int primary key auto_increment, usuario varchar(50),pass varchar(50),nombre varchar(100));

/*VISTA USUARIOS*/
create view v_usuario as
select id,usuario,nombre from admin;

#region PROCEDURES Usuarios
/*INSERTAR*/
drop procedure if exists p_inUsuario;
create procedure p_inUsuario
(in _usuario varchar(50), in _pass varchar(50),in _nombre varchar(100))
  begin
  insert into admin values(null,_usuario,_pass,_nombre);
  end;
  call p_inUsuario('Chxrlie',123,'Carlos');
  select * from admin;
  
  /*BORRAR*/
  drop procedure if exists p_delUsuario;
  create procedure p_delUsuario
  (in _id int)
  begin
  delete from admin where id = _id;
  end;
  call p_delUsuario(1);
  
  /*ACTUALIZAR*/
  drop procedure if exists p_modUsuario;
  create procedure p_modUsuario
  (in _usuario varchar(50),in _pass varchar(50), in _nombre varchar(100),in _id int)
  begin
  update admin set usuario = _usuario, pass = _pass, nombre = _nombre where id = _id;
  end;
  call p_modusuario('Carlos','12345','Juan',2);
  
  

#end region

#end region

#region USUARIO BD

create user 'Pedro'@'localhost' identified by '12345';
GRANT ALL PRIVILEGES ON Club_Ajedrez2.* TO 'Pedro'@'localhost';

#end region
#END REGION
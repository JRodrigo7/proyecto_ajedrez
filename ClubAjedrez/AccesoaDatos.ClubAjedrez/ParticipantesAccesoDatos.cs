﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace AccesoaDatos.ClubAjedrez
{
    public class ParticipantesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ParticipantesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }
        public List<Participantes> GetParticipantes(string filtro)
        {

            var listparticipantes = new List<Participantes>();
            var ds = new DataSet();
            string consulta = "Select * from v_participantes where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_participantes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participantes
                {
                    Idparticipante = Convert.ToInt32(row["idparticipante"]),
                    Numsocio = row["Socio"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apellidopaterno = row["Ap.paterno"].ToString(),
                    Apellidomaterno = row["Ap.Materno"].ToString(),
                    Direcccion = row["Direccion"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Pais = row["pais"].ToString()

                };
                listparticipantes.Add(participante);
            }
            return listparticipantes;
        }

        public List<Participantes> GetParticipantes2(ComboBox cm)
        {
            var listparticipante = new List<Participantes>();
            var ds = new DataSet();
            string consulta = "select idparticipante from participantes ";
            ds = conexion.ObtenerDatos(consulta, "participantes");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idparticipante";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var particiante = new Participantes
                {


                    Idparticipante = Convert.ToInt32(row["idparticipante"]),


                };
                listparticipante.Add(particiante);
            }
            return listparticipante;
        }
    }
}

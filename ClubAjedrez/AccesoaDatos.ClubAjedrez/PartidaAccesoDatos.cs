﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class PartidaAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public PartidaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar( Partida partida)
        {
            if (partida.Idpartida == 0)
            {
                string consulta = string.Format("call p_inPartida('{0}','{1}','{2}','{3}','{4}','{5}')",
                partida.Jugadorb, partida.Jugadorn, partida.Arbitro, partida.Sala, partida.Fecha, partida.Jornada);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_partidas('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                partida.Jugadorb, partida.Jugadorn, partida.Arbitro, partida.Sala, partida.Fecha, partida.Jornada ,partida.Idpartida);
                conexion.EjecutarConsulta(consulta);
            }
        }

        public void Eliminar(int idpartida)
        {
            string consulta = string.Format("call eliminar_partidas('{0}')", idpartida);
            conexion.EjecutarConsulta(consulta);
        }

        public List<VistaPartida> GetPartidas (string filtro)
        {
            var listvistapartida = new List<VistaPartida>();
            var ds = new DataSet();
            string consulta = "select * from v_partidas;";

            ds = conexion.ObtenerDatos(consulta, "partidas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var vistapartida = new VistaPartida
                {
                    IdPartida = Convert.ToInt32(row["idpartida"]),
                    Jugador_Blancas = row["Jugador_Blancas"].ToString(),
                    Jugador_Negras = row["Jugador_Negras"].ToString(),
                    Arbitro = row["Arbitro"].ToString(),
                    Sala = Convert.ToInt32(row["Sala"]),
                    Fecha = row["Fecha"].ToString(),
                    Jornada = row["Jornada"].ToString(),

                };
                listvistapartida.Add(vistapartida);
            }
            return listvistapartida;
        }

        public List<VistaSalas> GetHoltelSala(string filtro)
        {

            var listsalash = new List<VistaSalas>();
            var ds = new DataSet();
            string consulta = "Select * from v_sala where Hotel like '%" + filtro+"%';";

            ds = conexion.ObtenerDatos(consulta, "salas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var salas = new VistaSalas
                {
                    Idsala = Convert.ToInt32(row["idsala"]),
                    Numero_de_sala = Convert.ToInt32(row["numsala"]),
                    Capacidad = Convert.ToInt32(row["capacidad"]),
                    Medios = row["medios"].ToString(),
                    Nombre_Hotel = row["Hotel"].ToString(),


                };
                listsalash.Add(salas);
            }
            return listsalash;
        }
    }
}

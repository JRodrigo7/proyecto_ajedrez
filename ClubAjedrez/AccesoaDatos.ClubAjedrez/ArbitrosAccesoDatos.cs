﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class ArbitrosAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ArbitrosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Arbitros arbitros)
        {
            if (arbitros.Idarbitro == 0)
            {

                string consulta = string.Format("call p_inArbitroP('{0}','{1}','{2}','{3}','{4}','{5}','{6}') ",
                    arbitros.Numsocio, arbitros.Nombre, arbitros.Apellidopaterno, arbitros.Apellidomaterno, arbitros.Direcccion, arbitros.Telefono, arbitros.Pais);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarArbitroP('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                    arbitros.Numsocio, arbitros.Nombre, arbitros.Apellidopaterno, arbitros.Apellidomaterno, arbitros.Direcccion, arbitros.Telefono, arbitros.Pais, arbitros.Idarbitro);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idarbitro)
        {
            string consulta = string.Format("call p_delArbitroP('{0}')", idarbitro);
            conexion.EjecutarConsulta(consulta);
        }

        public List<VistaArbitros> GetArbitros(string filtro)
        {

            var listarbitros = new List<VistaArbitros>();
            var ds = new DataSet();
            string consulta = "Select * from v_arbitros where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_arbitros");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var arbitro = new VistaArbitros
                {
                    IdArbitro = Convert.ToInt32(row["idarbitro"]),
                    Numsocio = row["Socio"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apellidopaterno = row["Ap.paterno"].ToString(),
                    Apellidomaterno = row["Ap.Materno"].ToString(),
                    Direcccion = row["Direccion"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Pais = row["pais"].ToString()

                };
                listarbitros.Add(arbitro);
            }
            return listarbitros;
        }

        public List<VistaArbitros2> GetArbitros2(string filtro)
        {

            var listarbitros = new List<VistaArbitros2>();
            var ds = new DataSet();
            string consulta = "Select * from v_arbitros2 where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_arbitros2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var arbitro = new VistaArbitros2
                {
                    IdArbitro = Convert.ToInt32(row["idarbitro"]),
                    Numsocio = row["Socio"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Direcccion = row["Direccion"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Pais = row["pais"].ToString()

                };
                listarbitros.Add(arbitro);
            }
            return listarbitros;
        }

    }
}

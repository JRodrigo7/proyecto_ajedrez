﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class MovimientoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MovimientoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Movimiento movimiento)
        {
            if (movimiento.IdMovimiento == 0)
            {
                string consulta = string.Format("call insertar_movimientos(null,'{0}','{1}','{2}','{3}')",
                movimiento.NumMovimiento,movimiento.Posiciones,movimiento.Descripcion,movimiento.fkPartida);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_movimientos('{0}','{1}','{2}','{3}','{4}')",
                movimiento.NumMovimiento, movimiento.Posiciones, movimiento.Descripcion, movimiento.fkPartida, movimiento.IdMovimiento);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idmovimiento)
        {
            string consulta = string.Format("call eliminar_movimientos('{0}')", idmovimiento);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Movimiento> GetMovimientos(string filtro)
        {

            var listmovimiento = new List<Movimiento>();
            var ds = new DataSet();
            string consulta = "Select * from movimientos";

            ds = conexion.ObtenerDatos(consulta, "movimientos");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var movimiento = new Movimiento
                {
                    IdMovimiento = Convert.ToInt32(row["idmovimiento"]),
                    NumMovimiento = Convert.ToInt32(row["nummovimiento"]),
                    Posiciones = row["posiciones"].ToString(),
                    Descripcion = row["descripcion"].ToString(),
                    fkPartida = Convert.ToInt32(row["fkpartida"]),
                };
                listmovimiento.Add(movimiento);
            }
            return listmovimiento;
        }
    }
}

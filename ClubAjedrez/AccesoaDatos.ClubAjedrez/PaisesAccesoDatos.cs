﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class PaisesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public PaisesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Paises paises)
        {
            if (paises.Idpais == 0)
            {
                
                string consulta = string.Format("call p_insertarPaises('{0}','{1}','{2}','{3}') ",
                    paises.Idpais, paises.Nombrepais, paises.Numeroclubes, paises.Fkpais);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_paises('{0}','{1}','{2}','{3}')",
                    paises.Nombrepais, paises.Numeroclubes, paises.Fkpais, paises.Idpais);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idpais)
        {
            string consulta = string.Format("call eliminar_paises('{0}')", idpais);
            conexion.EjecutarConsulta(consulta);
        }


        public List<PaisesVista> GetVistas(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listpaises = new List<PaisesVista>();
            var ds = new DataSet();
            string consulta = "Select * from v_paises where pais like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_paises");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var pais= new PaisesVista
                {
                    Id = Convert.ToInt32(row["id"]),
                    Pais = row["pais"].ToString(),
                    Clubes = Convert.ToInt32(row["clubes"]),
                    Paisrep = row["Pais Representante"].ToString()

                };
                listpaises.Add(pais);
            }
            return listpaises;
        }

        public List<Paises> GetPaises2(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listpaises2 = new List<Paises>();
            var ds = new DataSet();
            string consulta = "Select * from paises where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "paises");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var pais2 = new Paises
                {
                    Idpais = Convert.ToInt32(row["idpais"]),
                    Nombrepais = row["nombre"].ToString(),
                    Numeroclubes = Convert.ToInt32(row["numeroclub"]),
                    Fkpais = row["paisrepresentante"].ToString()

                };
                listpaises2.Add(pais2);
            }
            return listpaises2;
        }

        
    }
}

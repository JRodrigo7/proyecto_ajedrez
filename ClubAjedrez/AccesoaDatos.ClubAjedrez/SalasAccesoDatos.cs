﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class SalasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public SalasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Salas sala)
        {
            if (sala.Ids == 0)
            {

                string consulta = string.Format("call insertar_salas(null,'{0}','{1}','{2}','{3}') ",
                    sala.Numsala, sala.Cap, sala.Med, sala.Cod);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_salas('{0}','{1}','{2}','{3}','{4}')",
                    sala.Numsala, sala.Cap, sala.Med, sala.Cod, sala.Ids);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idsala)
        {
            string consulta = string.Format("call eliminar_salas('{0}')", idsala);
            conexion.EjecutarConsulta(consulta);
        }

        public List<VistaSalas> Getsalas(string filtro)
        {

            var listsalas = new List<VistaSalas>();
            var ds = new DataSet();
            string consulta = "select * from v_sala;";

            ds = conexion.ObtenerDatos(consulta, "salas");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var salas = new VistaSalas
                {
                    Idsala = Convert.ToInt32(row["idsala"]),
                    Numero_de_sala = Convert.ToInt32(row["numsala"]),
                    Capacidad = Convert.ToInt32(row["capacidad"]),
                    Medios = row["medios"].ToString(),
                    Nombre_Hotel = row["Hotel"].ToString(),


                };
                listsalas.Add(salas);
            }
            return listsalas;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace AccesoaDatos.ClubAjedrez
{
    public class AdminAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AdminAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Login admin)
        {
            if (admin.Id == 0)
            {

                string consulta = string.Format("call p_inUsuario('{0}','{1}','{2}') ",
                    admin.Usuario, admin.Pass, admin.Nombre);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_modUsuario('{0}','{1}','{2}','{3}')",
                    admin.Usuario, admin.Pass,admin.Nombre, admin.Id);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idadmin)
        {
            string consulta = string.Format("call p_delUsuario('{0}')", idadmin);
            conexion.EjecutarConsulta(consulta);
        }

        public DataSet Consulta(int id)
        {
            var ds = new DataSet();
            string consulta = "Select * from admin where id = '" + id + "'";

            ds = conexion.ObtenerDatos(consulta, "admin");

            return ds;
        }
        public DataSet ConsultaUsuario(string usuario)
        {
            var ds = new DataSet();
            string consulta = "Select * from admin where usuario = '" + usuario + "'";

            ds = conexion.ObtenerDatos(consulta, "admin");

            return ds;
        }
        public DataSet ConsultaPass(string pass)
        {
            var ds = new DataSet();
            string consulta = "Select * from admin where pass = '" + pass + "'";

            ds = conexion.ObtenerDatos(consulta, "admin");

            return ds;
        }

        public List<VistaLogin> GetAdmins(string filtro)
        {

            var listadmins = new List<VistaLogin>();
            var ds = new DataSet();
            string consulta = "Select * from v_usuario where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_usuario");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var admin = new VistaLogin
                {
                    Id = Convert.ToInt32(row["id"]),
                    Usuario = row["usuario"].ToString(),
                    Nombre = row["Nombre"].ToString()
                   

                };
                listadmins.Add(admin);
            }
            return listadmins;
        }
    }
}

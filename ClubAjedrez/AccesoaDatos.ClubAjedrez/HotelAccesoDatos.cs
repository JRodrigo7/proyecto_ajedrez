﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace AccesoaDatos.ClubAjedrez
{
    public class HotelAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public HotelAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Hoteles hoteles)
        {
            if (hoteles.Idhotel == 0)
            {

                string consulta = string.Format("call insertar_hoteles(null,'{0}','{1}','{2}') ",
                    hoteles.Nombre, hoteles.Direccion, hoteles.Telefono);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_hoteles('{0}','{1}','{2}','{3}')",
                    hoteles.Nombre, hoteles.Direccion, hoteles.Telefono, hoteles.Idhotel);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idhotel)
        {
            string consulta = string.Format("call eliminar_hoteles('{0}')", idhotel);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Hoteles> Gethoteles(string filtro)
        {

            var listhoteles = new List<Hoteles>();
            var ds = new DataSet();
            string consulta = "Select * from hoteles";

            ds = conexion.ObtenerDatos(consulta, "hoteles");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hoteles
                {
                    Idhotel = Convert.ToInt32(row["idhotel"]),
                    Nombre = row["nombre"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Telefono = row["telefono"].ToString(),
                 

                };
                listhoteles.Add(hoteles);
            }
            return listhoteles;
        }
        public List<Hoteles> Gethotelcmb(ComboBox cm)
        {
            var listhotel = new List<Hoteles>();
            var ds = new DataSet();
            string consulta = "select nombre from hoteles ";
            ds = conexion.ObtenerDatos(consulta, "hoteles");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hoteles
                {


                   Nombre =  row["nombre"].ToString(),


                };
                listhotel.Add(hoteles);
            }
            return listhotel;
        }
    }
}


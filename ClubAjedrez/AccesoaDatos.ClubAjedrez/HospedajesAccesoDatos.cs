﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace AccesoaDatos.ClubAjedrez
{
    public class HospedajesAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public HospedajesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Hospedajes hospedaje)
        {
            if (hospedaje.Idhospedaje == 0)
            {

                string consulta = string.Format("call insertar_hospedajes(null,'{0}','{1}','{2}','{3}') ",
                    hospedaje.Fkparticiapnte, hospedaje.Fkhotel, hospedaje.Fechainicial, hospedaje.Fechafinal);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call actualizar_hospedajes('{0}','{1}','{2}','{3}','{4}')",
                  hospedaje.Fkparticiapnte, hospedaje.Fkhotel, hospedaje.Fechainicial, hospedaje.Fechafinal, hospedaje.Idhospedaje);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idhospedaje)
        {
            string consulta = string.Format("call eliminar_hospedaje('{0}')", idhospedaje);
            conexion.EjecutarConsulta(consulta);
        }

        public List<VistaHospedajes> GetHospedajes(string filtro)
        {

            var listho = new List<VistaHospedajes>();
            var ds = new DataSet();
            string consulta = "Select * from v_hospedaje";

            ds = conexion.ObtenerDatos(consulta, "hospedajes");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hospedajes = new VistaHospedajes
                {
                    Idhospedaje = Convert.ToInt32(row["idhospedaje"]),
                    Apellidos = row["Apellidos"].ToString(),
                    Participante = row["Participante"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Fechainicial = row["fechainicial"].ToString(),
                    Fechafinal = row["fechafinal"].ToString(),
                    


                };
                listho.Add(hospedajes);
            }
            return listho;
        }
        public List<Hoteles> Gethotel2(ComboBox cm)
        {
            var listhotel = new List<Hoteles>();
            var ds = new DataSet();
            string consulta = "select nombre from hoteles ";
            ds = conexion.ObtenerDatos(consulta, "hoteles");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "nombre";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var hoteles = new Hoteles
                {


                   Nombre = row["nombre"].ToString(),



                };
                listhotel.Add(hoteles);
            }
            return listhotel;
        }
        public List<Participantes> Getparticipantes2(ComboBox cm)
        {
            var listp = new List<Participantes>();
            var ds = new DataSet();
            string consulta = "select idparticipante from participantes";
            ds = conexion.ObtenerDatos(consulta, "participantes");
            cm.DataSource = ds.Tables[0];
            cm.DisplayMember = "idparticipante";
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participantes
                {


                    Idparticipante = Convert.ToInt32(row["idparticipante"]),


                };
                listp.Add(participante);
            }
            return listp;
        }
    }
}

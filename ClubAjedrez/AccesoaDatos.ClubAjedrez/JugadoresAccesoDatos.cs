﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ClubAjedrez;

namespace AccesoaDatos.ClubAjedrez
{
    public class JugadoresAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public JugadoresAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "Club_Ajedrez2", 3306);
        }

        public void Guardar(Jugadores jugadores)
        {
            if (jugadores.Idjugador == 0)
            {

                string consulta = string.Format("call p_inJugadorP('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}') ",
                    jugadores.Numsocio, jugadores.Nombre, jugadores.Apellidopaterno, jugadores.Apellidomaterno, jugadores.Direcccion, jugadores.Telefono, jugadores.Pais, jugadores.Nivel);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("call p_actualizarJugadorP('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                    jugadores.Numsocio, jugadores.Nombre, jugadores.Apellidopaterno, jugadores.Apellidomaterno, jugadores.Direcccion, jugadores.Telefono, jugadores.Pais, jugadores.Idjugador, jugadores.Nivel);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idjugador)
        {
            string consulta = string.Format("call p_delJugadorP('{0}')", idjugador);
            conexion.EjecutarConsulta(consulta);
        }

        public List<VistaJugadores> GetJugadores(string filtro)
        {
            
            var listjugador = new List<VistaJugadores>();
            var ds = new DataSet();
            string consulta = "Select * from v_jugadores where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_jugadores");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jugador = new VistaJugadores
                {
                    Idjugador = Convert.ToInt32(row["idjugador"]),
                    Numsocio = row["Socio"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apellidopaterno = row["Ap.paterno"].ToString(),
                    Apellidomaterno = row["Ap.Materno"].ToString(),
                    Direcccion = row["Direccion"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Nivel = Convert.ToInt32(row["Nivel"]),
                    Pais = row["pais"].ToString()

                };
                listjugador.Add(jugador);
            }
            return listjugador;
        }

        public List<VistaJugadores2> GetJugadores2(string filtro)
        {

            var listjugador = new List<VistaJugadores2>();
            var ds = new DataSet();
            string consulta = "Select * from v_jugadores3 where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_jugadores3");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var jugador2 = new VistaJugadores2
                {
                    Idjugador = Convert.ToInt32(row["idjugador"]),
                    Numsocio = row["Socio"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Direcccion = row["Direccion"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Nivel = Convert.ToInt32(row["Nivel"]),
                    Pais = row["pais"].ToString()

                };
                listjugador.Add(jugador2);
            }
            return listjugador;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Salas
    {
        private int _Ids;
        private int _Numsala;
        private int _Cap;
        private string _Med;
        private int _Cod;

        public int Ids { get => _Ids; set => _Ids = value; }
        public int Numsala { get => _Numsala; set => _Numsala = value; }
        public int Cap { get => _Cap; set => _Cap = value; }
        public string Med { get => _Med; set => _Med = value; }
        public int Cod { get => _Cod; set => _Cod = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaJugadores
    {
        private int _idjugador;
        private string _numsocio;
        private string _nombre;
        private string _apellidopaterno;
        private string _apellidomaterno;
        private string _direcccion;
        private string _telefono;
        private int _nivel;
        private string _pais;
        

        public int Idjugador { get => _idjugador; set => _idjugador = value; }
        public string Numsocio { get => _numsocio; set => _numsocio = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellidopaterno { get => _apellidopaterno; set => _apellidopaterno = value; }
        public string Apellidomaterno { get => _apellidomaterno; set => _apellidomaterno = value; }
        public string Direcccion { get => _direcccion; set => _direcccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public int Nivel { get => _nivel; set => _nivel = value; }
        public string Pais { get => _pais; set => _pais = value; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaHospedajes
    {
        private int _idhospedaje;
        private string _apellidos;
        private string _participante;
        private string _nombre;
        private string _fechainicial;
        private string _fechafinal;

        public int Idhospedaje { get => _idhospedaje; set => _idhospedaje = value; }
        public string Apellidos { get => _apellidos; set => _apellidos = value; }
        public string Participante { get => _participante; set => _participante = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Fechainicial { get => _fechainicial; set => _fechainicial = value; }
        public string Fechafinal { get => _fechafinal; set => _fechafinal = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
   public class Hoteles
    {
        private int _Idhotel;
        private string _Nombre;
        private string _Direccion;
        private string _Telefono;

        public int Idhotel { get => _Idhotel; set => _Idhotel = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Login
    {
        private int id;
        private string _usuario;
        private string _pass;
        private string _nombre;

        public int Id { get => id; set => id = value; }
        public string Usuario { get => _usuario; set => _usuario = value; }
        public string Pass { get => _pass; set => _pass = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }


    }
}

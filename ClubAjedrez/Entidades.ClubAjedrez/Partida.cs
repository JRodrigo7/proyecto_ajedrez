﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Partida
    {
        private int _Idpartida;
        private int _Jugadorb;
        private int _Jugadorn;
        private int _Arbitro;
        private int _Sala;
        private string _Fecha;
        private string _Jornada;

        public int Idpartida { get => _Idpartida; set => _Idpartida = value; }
        public int Jugadorb { get => _Jugadorb; set => _Jugadorb = value; }
        public int Jugadorn { get => _Jugadorn; set => _Jugadorn = value; }
        public int Arbitro { get => _Arbitro; set => _Arbitro = value; }
        public int Sala { get => _Sala; set => _Sala = value; }
        public string Fecha { get => _Fecha; set => _Fecha = value; }
        public string Jornada { get => _Jornada; set => _Jornada = value; }
    }
}

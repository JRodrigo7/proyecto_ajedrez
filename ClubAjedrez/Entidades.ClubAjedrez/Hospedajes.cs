﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Hospedajes
    {
        private int _idhospedaje;
        private int _fkparticiapnte;
        private int _fkhotel;
        private string _fechainicial;
        private string _fechafinal;

        public int Idhospedaje { get => _idhospedaje; set => _idhospedaje = value; }
        public int Fkparticiapnte { get => _fkparticiapnte; set => _fkparticiapnte = value; }
        public int Fkhotel { get => _fkhotel; set => _fkhotel = value; }
        public string Fechainicial { get => _fechainicial; set => _fechainicial = value; }
        public string Fechafinal { get => _fechafinal; set => _fechafinal = value; }
    }
}

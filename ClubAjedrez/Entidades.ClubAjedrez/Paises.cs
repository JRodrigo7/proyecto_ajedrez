﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Paises
    {
        private int _idpais;
        private string _nombrepais;
        private int _numeroclubes;
        private string _fkpais;

        

        public int Idpais { get => _idpais; set => _idpais = value; }
        public string Nombrepais { get => _nombrepais; set => _nombrepais = value; }
        public int Numeroclubes { get => _numeroclubes; set => _numeroclubes = value; }
        public string Fkpais { get => _fkpais; set => _fkpais = value; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Movimiento
    {
        private int _IdMovimiento;
        private int _NumMovimiento;
        private string _Posiciones;
        private string _Descripcion;
        private int _fkPartida;

        public int IdMovimiento { get => _IdMovimiento; set => _IdMovimiento = value; }
        public int NumMovimiento { get => _NumMovimiento; set => _NumMovimiento = value; }
        public string Posiciones { get => _Posiciones; set => _Posiciones = value; }
        public string Descripcion { get => _Descripcion; set => _Descripcion = value; }
        public int fkPartida { get => _fkPartida; set => _fkPartida = value; }
    }
}

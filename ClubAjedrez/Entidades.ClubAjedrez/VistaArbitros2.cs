﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaArbitros2
    {
        private int _idArbitro;
        private string _numsocio;
        private string _nombre;
        private string _direcccion;
        private string _telefono;
        private string _pais;

        public int IdArbitro { get => _idArbitro; set => _idArbitro = value; }
        public string Numsocio { get => _numsocio; set => _numsocio = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Direcccion { get => _direcccion; set => _direcccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Pais { get => _pais; set => _pais = value; }
    }
}

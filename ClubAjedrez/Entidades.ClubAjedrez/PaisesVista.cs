﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class PaisesVista
    {
        private int _id;
        private string _pais;
        private int _clubes;
        private string _paisrep;

        public int Id { get => _id; set => _id = value; }
        public string Pais { get => _pais; set => _pais = value; }
        public int Clubes { get => _clubes; set => _clubes = value; }
        public string Paisrep { get => _paisrep; set => _paisrep = value; }
    }
}

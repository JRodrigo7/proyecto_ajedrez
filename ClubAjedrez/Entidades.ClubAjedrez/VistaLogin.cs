﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaLogin
    {
        private int id;
        private string _usuario;
        private string _nombre;

        public int Id { get => id; set => id = value; }
        public string Usuario { get => _usuario; set => _usuario = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}

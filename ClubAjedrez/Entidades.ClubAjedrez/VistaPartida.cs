﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaPartida
    {
        private int _IdPartida;
        private string _Jugador_Blancas;
        private string _Jugador_Negras;
        private string _Arbitro;
        private int _Sala;
        private string _Fecha;
        private string _Jornada;

        public int IdPartida {get => _IdPartida; set => _IdPartida = value;}
        public string Jugador_Blancas {get => _Jugador_Blancas; set => _Jugador_Blancas = value;}
        public string Jugador_Negras {get => _Jugador_Negras; set => _Jugador_Negras = value;}
        public string Arbitro {get => _Arbitro; set => _Arbitro = value;}
        public int Sala { get => _Sala; set => _Sala = value; }
        public string Fecha {get => _Fecha; set => _Fecha = value;}
        public string Jornada {get => _Jornada; set => _Jornada = value;}

    }
}

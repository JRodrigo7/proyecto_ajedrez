﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class VistaSalas
    {
        private int _Idsala;
        private int _Numero_de_sala;
        private int _Capacidad;
        private string _Medios;
        private string _Nombre_Hotel;

        public int Idsala { get => _Idsala; set => _Idsala = value; }
        public int Numero_de_sala { get => _Numero_de_sala; set => _Numero_de_sala = value; }
        public int Capacidad { get => _Capacidad; set => _Capacidad = value; }
        public string Medios { get => _Medios; set => _Medios = value; }
        public string Nombre_Hotel { get => _Nombre_Hotel; set => _Nombre_Hotel = value; }
    }
}

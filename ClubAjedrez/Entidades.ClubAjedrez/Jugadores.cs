﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ClubAjedrez
{
    public class Jugadores
    {
        private int idjugador;
        private int nivel;
        private int idparticipante;
        private string numsocio;
        private string nombre;
        private string apellidopaterno;
        private string apellidomaterno;
        private string direcccion;
        private string telefono;
        private int pais;

        public int Idjugador { get => idjugador; set => idjugador = value; }
        public int Nivel { get => nivel; set => nivel = value; }
        public int Idparticipante { get => idparticipante; set => idparticipante = value; }
        public string Numsocio { get => numsocio; set => numsocio = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidopaterno { get => apellidopaterno; set => apellidopaterno = value; }
        public string Apellidomaterno { get => apellidomaterno; set => apellidomaterno = value; }
        public string Direcccion { get => direcccion; set => direcccion = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public int Pais { get => pais; set => pais = value; }
    }
}

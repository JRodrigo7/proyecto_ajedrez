﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;

namespace LogicaNegocio.ClubAjedrez
{
    public class MovimientoManejador
    {
        private MovimientoAccesoDatos _movimientoAccesoDatos = new MovimientoAccesoDatos();
        public void Guardar(Movimiento movimiento)
        {
            _movimientoAccesoDatos.Guardar(movimiento);
        }
        public void Eliminar(int idmovimiento)
        {
            _movimientoAccesoDatos.Eliminar(idmovimiento);
        }
        public List<Movimiento> GetMovimientos(string filtro)
        {
            var listmovimiento = _movimientoAccesoDatos.GetMovimientos(filtro);
            return listmovimiento;
        }
    }
}

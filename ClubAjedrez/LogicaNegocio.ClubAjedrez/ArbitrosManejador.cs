﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;

namespace LogicaNegocio.ClubAjedrez
{
    public class ArbitrosManejador
    {
        private ArbitrosAccesoDatos _arbitrosAccesoDatos = new ArbitrosAccesoDatos();
        public void Guardar(Arbitros arbitros)
        {
            _arbitrosAccesoDatos.Guardar(arbitros);

        }
        public void Eliminar(int idarbitro)
        {
            
            _arbitrosAccesoDatos.Eliminar(idarbitro);
        }
        public List<VistaArbitros> GetArbitros(string filtro)
        {
            
            var listarbitro = _arbitrosAccesoDatos.GetArbitros(filtro);
          
            return listarbitro;
        }
        public List<VistaArbitros2> GetArbitros2(string filtro)
        {

            var listarbitro = _arbitrosAccesoDatos.GetArbitros2(filtro);

            return listarbitro;
        }
    }
}

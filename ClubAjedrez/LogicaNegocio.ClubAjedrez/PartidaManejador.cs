﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;

namespace LogicaNegocio.ClubAjedrez
{
    public class PartidaManejador
    {
        private PartidaAccesoDatos partidaAccesoDatos = new PartidaAccesoDatos();

        public void Guardar(Partida partida)
        {
            partidaAccesoDatos.Guardar(partida);
        }
        public void Eliminar(int idpartida)
        {
            partidaAccesoDatos.Eliminar(idpartida);
        }
        public List<VistaPartida> GetPartidas(string filtro)
        {
            var vistalistpartida = partidaAccesoDatos.GetPartidas(filtro);
            return vistalistpartida;
        }
        public List<VistaSalas> GetHoltelSala(string filtro)
        {
            var listsalash = partidaAccesoDatos.GetHoltelSala(filtro);
            return listsalash;
        }
    }
}

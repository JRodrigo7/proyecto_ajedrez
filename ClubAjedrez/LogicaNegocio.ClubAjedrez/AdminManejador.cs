﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;
using System.Data;

namespace LogicaNegocio.ClubAjedrez
{
    public class AdminManejador
    {
        private AdminAccesoDatos _adminAccesoDatos = new AdminAccesoDatos();
        public void Guardar(Login admin)
        {
            _adminAccesoDatos.Guardar(admin);

        }
        public void Eliminar(int idadmin)
        {

            _adminAccesoDatos.Eliminar(idadmin);
        }
        public List<VistaLogin> GetAdmins(string filtro)
        {

            var listarbitro = _adminAccesoDatos.GetAdmins(filtro);

            return listarbitro;
        }
        public DataSet consulta(int id)
        {
            DataSet dt = _adminAccesoDatos.Consulta(id);

            return dt;
        }
        public DataSet consultaUsuario(string usuario)
        {
            DataSet dt = _adminAccesoDatos.ConsultaUsuario(usuario);

            return dt;
        }
        public DataSet consultaPass(string pass)
        {
            DataSet dt = _adminAccesoDatos.ConsultaPass(pass);

            return dt;
        }
    }
}

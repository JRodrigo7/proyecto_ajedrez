﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;
using System.Data;

namespace LogicaNegocio.ClubAjedrez
{
    public class PaisesManejador
    {
        private PaisesAccesoDatos _paisesAccesoDatos = new PaisesAccesoDatos();
        public void Guardar(Paises paises)
        {
            _paisesAccesoDatos.Guardar(paises);

        }
        public void Eliminar(int idpais)
        {
            //eliminar
            _paisesAccesoDatos.Eliminar(idpais);
        }
        public List<PaisesVista> GetVistas(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listPais = _paisesAccesoDatos.GetVistas(filtro);
            //Llenar lista
            return listPais;
        }
        public List<Paises> GetPaises2(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listPais2 = _paisesAccesoDatos.GetPaises2(filtro);
            //Llenar lista
            return listPais2;
        }
        
    }
}

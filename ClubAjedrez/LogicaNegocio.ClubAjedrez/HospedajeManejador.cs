﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace LogicaNegocio.ClubAjedrez
{
    public class HospedajeManejador
    {
        private HospedajesAccesoDatos hospedajesAcceso = new HospedajesAccesoDatos();
        public void Guardar(Hospedajes hospedaje)
        {
            hospedajesAcceso.Guardar(hospedaje);

        }
        public void Eliminar(int idhospedaje)
        { 
            hospedajesAcceso.Eliminar(idhospedaje);
        }
        public List<VistaHospedajes> GetHospedajes(string filtro)
        {

            var listhospedajes = hospedajesAcceso.GetHospedajes(filtro);

            return listhospedajes;
        }
        public List<Hoteles> gethoteles2(ComboBox cm)
        {
            var listhoteles = hospedajesAcceso.Gethotel2(cm);

            return listhoteles;
        }
        public List<Participantes> GetParticipantes2(ComboBox cm)
        {
            var listparticipante = hospedajesAcceso.Getparticipantes2(cm);

            return listparticipante;
        }
    }
}

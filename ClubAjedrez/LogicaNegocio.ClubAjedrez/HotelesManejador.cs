﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;
using System.Windows.Forms;


namespace LogicaNegocio.ClubAjedrez
{
    public class HotelesManejador
    {
        private HotelAccesoDatos _HotelAccesoDatos = new HotelAccesoDatos();
        public void Guardar(Hoteles hoteles)
        {
            _HotelAccesoDatos.Guardar(hoteles);

        }
        public void Eliminar(int idhotel)
        {

            _HotelAccesoDatos.Eliminar(idhotel);
        }
        public List<Hoteles> GetHoteles(string filtro)
        {

            var listhotel = _HotelAccesoDatos.Gethoteles(filtro);

            return listhotel;
        }
        public List<Hoteles> gethotelescm(ComboBox cm)
        {
            var listhoteles = _HotelAccesoDatos.Gethotelcmb(cm);

            return listhoteles;
        }
    }
}

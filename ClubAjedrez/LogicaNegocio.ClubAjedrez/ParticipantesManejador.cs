﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;
using System.Windows.Forms;

namespace LogicaNegocio.ClubAjedrez
{
    public class ParticipantesManejador
    {
        private ParticipantesAccesoDatos _participantesaccesodatos = new ParticipantesAccesoDatos();

        public List<Participantes> GetParticipantes(string filtro)
        {

            var listparticipante = _participantesaccesodatos.GetParticipantes(filtro);

            return listparticipante;
        }
        
        public List<Participantes> GetParticipantes2(ComboBox cm)
        {
            var listp = _participantesaccesodatos.GetParticipantes2(cm);

            return listp;
        }
    }

}

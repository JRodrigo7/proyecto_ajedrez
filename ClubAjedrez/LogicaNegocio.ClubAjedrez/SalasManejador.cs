﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;

namespace LogicaNegocio.ClubAjedrez
{
    public class SalasManejador
    {
        private SalasAccesoDatos salasAccesoDatos = new SalasAccesoDatos();
        public void Guardar(Salas salas)
        {
            salasAccesoDatos.Guardar(salas);

        }
        public void Eliminar(int idsala)
        {

            salasAccesoDatos.Eliminar(idsala);
        }
        public List<VistaSalas> GetSalas(string filtro)
        {

            var listsala = salasAccesoDatos.Getsalas(filtro);

            return listsala;
        }
    }
}

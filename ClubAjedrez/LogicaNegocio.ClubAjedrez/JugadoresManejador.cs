﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ClubAjedrez;
using Entidades.ClubAjedrez;


namespace LogicaNegocio.ClubAjedrez
{
    public class JugadoresManejador
    {

        private JugadoresAccesoDatos _jugadoresAccesoDatos = new JugadoresAccesoDatos();
        public void Guardar(Jugadores jugador)
        {
            _jugadoresAccesoDatos.Guardar(jugador);

        }
        public void Eliminar(int idjugador)
        {
            //eliminar
            _jugadoresAccesoDatos.Eliminar(idjugador);
        }
        public List<VistaJugadores> getJugadores(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listJugador = _jugadoresAccesoDatos.GetJugadores(filtro);
            //Llenar lista
            return listJugador;
        }
        public List<VistaJugadores2> GetJugadores2(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listJugador = _jugadoresAccesoDatos.GetJugadores2(filtro);
            //Llenar lista
            return listJugador;
        }


    }
}

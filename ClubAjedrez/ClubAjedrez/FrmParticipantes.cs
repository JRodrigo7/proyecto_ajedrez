﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmParticipantes : Form
    {
        private ParticipantesManejador _participanteManejador;
        public FrmParticipantes()
        {
            InitializeComponent();
            _participanteManejador = new ParticipantesManejador();
        }

        private void FrmParticipantes_Load(object sender, EventArgs e)
        {
            BuscarParticipantes("");
        }
        private void BuscarParticipantes(string filtro)
        {
            dgvParticipantes.DataSource = _participanteManejador.GetParticipantes(filtro);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarParticipantes(txtBuscar.Text);
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

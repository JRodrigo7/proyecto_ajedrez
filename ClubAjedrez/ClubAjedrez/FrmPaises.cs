﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmPaises : Form
    {
        private PaisesManejador _paisesManejador;
        public FrmPaises()
        {
            InitializeComponent();
            _paisesManejador = new PaisesManejador();

        }

        private void FrmPaises_Load(object sender, EventArgs e)
        {
            MostrarPais("");
            BuscarPaises("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void MostrarPais(string filtro)
        {
            cmbPaisr.DataSource = _paisesManejador.GetPaises2(filtro);

            cmbPaisr.ValueMember = "idpais";
            cmbPaisr.DisplayMember = "nombrepais";
            
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtPais.Enabled = activar;
            txtClubes.Enabled = activar;
            cmbPaisr.Enabled = activar;
            
        }
        private void LimpiarCuadros()
        {
            lblpais.Text = "0";
            txtPais.Text = "";
            txtClubes.Text = "";
            cmbPaisr.Text = "";
            
        }
        private void BuscarPaises(string filtro)
        {
            dgvPaises.DataSource = _paisesManejador.GetPaises2(filtro);
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtPais.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
           
            try
            {
                GuardarPais();
                LimpiarCuadros();
                BuscarPaises("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarPais()
        {
           

            _paisesManejador.Guardar(new Paises
            {
                Idpais = Convert.ToInt32(lblpais.Text),
                Nombrepais = txtPais.Text,
                Numeroclubes = Convert.ToInt32(txtClubes.Text),

                Fkpais = cmbPaisr.Text

            });;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPaises(txtBuscar.Text);
        }
        private void EliminarPais()
        {
            var idpais = dgvPaises.CurrentRow.Cells["idpais"].Value;
            _paisesManejador.Eliminar(Convert.ToInt32(idpais));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este País?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarPais();
                    BuscarPaises("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DgvPaises_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPais();
                BuscarPaises("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarPais()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblCod.Visible = true;
            

            lblpais.Text = dgvPaises.CurrentRow.Cells["idpais"].Value.ToString();
            txtPais.Text = dgvPaises.CurrentRow.Cells["nombrepais"].Value.ToString();
            txtClubes.Text = dgvPaises.CurrentRow.Cells["numeroclubes"].Value.ToString();
            cmbPaisr.Text = dgvPaises.CurrentRow.Cells["fkpais"].Value.ToString();
            
        }

        private void txtPais_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtClubes_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

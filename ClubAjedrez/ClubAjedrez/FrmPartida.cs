﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmPartida : Form
    {
        private PartidaManejador _partidaManejador;
        private JugadoresManejador _jugadoresManejador;
        private ArbitrosManejador _arbitrosManejador;
        private HotelesManejador _hotelesManejador;
        public FrmPartida()
        {
            InitializeComponent();
            _partidaManejador = new PartidaManejador();
            _jugadoresManejador = new JugadoresManejador();
            _arbitrosManejador = new ArbitrosManejador();
            _hotelesManejador = new HotelesManejador();
        }
        private void FrmPartida_Load(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            BuscarPartida("");
            _hotelesManejador.gethotelescm(cmbHotel);
            BuscarJugadorB("");
            BuscarJugadorN("");
            BuscarArbitro("");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbJugadorB.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarPartida();
                LimpiarCuadros();
                BuscarPartida("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar esta partida?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarPartida();
                    BuscarPartida("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            lblId.Enabled = activar;
            txtFechaPartida.Enabled = activar;
            cmbJugadorB.Enabled = activar;
            cmbJugadorN.Enabled = activar;
            cmbArbitro.Enabled = activar;
            cmbHotel.Enabled = activar;
            cmbSala.Enabled = activar;
            txtJornada.Enabled = activar;
        }

        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            txtFechaPartida.Text = "";
            cmbJugadorB.Text = "";
            cmbJugadorN.Text = "";
            cmbArbitro.Text = "";
            cmbHotel.Text = "";
            cmbSala.Text = "";
            txtJornada.Text = "";
        }

        private void BuscarPartida(string filtro)
        {
            dgvPartida.DataSource = _partidaManejador.GetPartidas(filtro);
        }

        private void GuardarPartida()
        {
            _partidaManejador.Guardar(new Partida
            {
                Idpartida = Convert.ToInt32(lblId.Text),
                Jugadorb = Convert.ToInt32(cmbJugadorB.SelectedValue),
                Jugadorn = Convert.ToInt32(cmbJugadorN.SelectedValue),
                Arbitro = Convert.ToInt32(cmbArbitro.SelectedValue),
                Sala = Convert.ToInt32(cmbSala.Text),
                Fecha = txtFechaPartida.Text,
                Jornada = txtJornada.Text,
            }); ;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarPartida(txtBuscar.Text);
        }

        private void EliminarPartida()
        {
            var idpartida = dgvPartida.CurrentRow.Cells["idpartida"].Value;
            _partidaManejador.Eliminar(Convert.ToInt32(idpartida));
        }

        private void dgvPartida_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarPartida();
                BuscarPartida("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ModificarPartida()
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            lblId.Text = dgvPartida.CurrentRow.Cells["idpartida"].Value.ToString();
            cmbJugadorB.Text= dgvPartida.CurrentRow.Cells["Jugador_Blancas"].Value.ToString();
            cmbJugadorN.Text = dgvPartida.CurrentRow.Cells["Jugador_Negras"].Value.ToString();
            cmbArbitro.Text = dgvPartida.CurrentRow.Cells["Arbitro"].Value.ToString();
            cmbSala.Text = dgvPartida.CurrentRow.Cells["Sala"].Value.ToString();
            txtFechaPartida.Text = dgvPartida.CurrentRow.Cells["Fecha"].Value.ToString();
            txtJornada.Text = dgvPartida.CurrentRow.Cells["Jornada"].Value.ToString();
        }

        private void BuscarJugadorB(string filtro)
        {
            cmbJugadorB.DataSource = _jugadoresManejador.GetJugadores2(filtro);
            cmbJugadorB.DisplayMember = "Nombre";
            cmbJugadorB.ValueMember = "idjugador";
            
        }
        private void BuscarJugadorN(string filtro)
        {
            cmbJugadorN.DataSource = _jugadoresManejador.GetJugadores2(filtro);
            cmbJugadorN.DisplayMember = "Nombre";
            cmbJugadorN.ValueMember = "idjugador";
        }
        private void BuscarArbitro(string filtro)
        {
            cmbArbitro.DataSource = _arbitrosManejador.GetArbitros2(filtro);
            cmbArbitro.DisplayMember = "Nombre";
            cmbArbitro.ValueMember = "idarbitro";
        }
        private void SalaHotel(string filtro)
        {
            cmbSala.DataSource = _partidaManejador.GetHoltelSala(filtro);
            cmbSala.DisplayMember = "idsala";
        }

        private void cmbHotel_SelectedIndexChanged(object sender, EventArgs e)
        { 
            SalaHotel(cmbHotel.Text);
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtFechaPartida_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtJornada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }
    }
}

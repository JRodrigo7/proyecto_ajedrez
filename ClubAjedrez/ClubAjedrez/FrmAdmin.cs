﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmAdmin : Form
    {
        private AdminManejador _adminmanejador;
        int ida;
        public FrmAdmin()
        {
            InitializeComponent();
            _adminmanejador = new AdminManejador();
        }

        private void FrmAdmin_Load(object sender, EventArgs e)
        {
            BuscarAdmins("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtUsuario.Enabled = activar;
            txtPass.Enabled = activar;
          

        }
        private void LimpiarCuadros()
        {
            ida = 0;

            txtNombre.Text = "";
            txtUsuario.Text = "";
            txtPass.Text = "";
            
        }
        private void BuscarAdmins(string filtro)
        {
            dgvAdmins.DataSource = _adminmanejador.GetAdmins(filtro);
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtUsuario.Focus();
        }
        private void GuardarAdmin()
        {
            _adminmanejador.Guardar(new Entidades.ClubAjedrez.Login
            {
                Id = ida,
                Usuario = txtUsuario.Text,
                Pass = txtPass.Text,
                Nombre = txtNombre.Text
                

            }); ;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAdmin();
                LimpiarCuadros();
                BuscarAdmins("");
                MessageBox.Show("Registro exitoso");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAdmins(txtBuscar.Text);
        }
        private void EliminarAdmin()
        {
            var idadmin = dgvAdmins.CurrentRow.Cells["id"].Value;
            _adminmanejador.Eliminar(Convert.ToInt32(idadmin));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este administrador?",
                "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAdmin();
                    BuscarAdmins("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ModificarAdmin()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            ida = Convert.ToInt32(dgvAdmins.CurrentRow.Cells["id"].Value.ToString());
            
            txtUsuario.Text = dgvAdmins.CurrentRow.Cells["Usuario"].Value.ToString();
            txtNombre.Text = dgvAdmins.CurrentRow.Cells["nombre"].Value.ToString();
            
        }
        private void ModificarPass()
        {
            var t = new DataSet();
            t = _adminmanejador.consulta(Convert.ToInt32(ida));
            txtPass.Text = t.Tables[0].Rows[0]["pass"].ToString();
        }

        private void DgvAdmins_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAdmin();
                ModificarPass();
                BuscarAdmins("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

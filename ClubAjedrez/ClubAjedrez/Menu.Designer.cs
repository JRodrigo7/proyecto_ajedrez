﻿namespace ClubAjedrez
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.panel2 = new System.Windows.Forms.Panel();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.iconPictureBox1 = new FontAwesome.Sharp.IconPictureBox();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.btnHospedaje = new FontAwesome.Sharp.IconButton();
            this.btnSalas = new FontAwesome.Sharp.IconButton();
            this.btnPaises = new FontAwesome.Sharp.IconButton();
            this.btnHotel = new FontAwesome.Sharp.IconButton();
            this.btnMovimientos = new FontAwesome.Sharp.IconButton();
            this.btnPartidas = new FontAwesome.Sharp.IconButton();
            this.btnArbitros = new FontAwesome.Sharp.IconButton();
            this.btnJugadores = new FontAwesome.Sharp.IconButton();
            this.btnParticipantes = new FontAwesome.Sharp.IconButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.iconButton1);
            this.panel2.Controls.Add(this.btnHospedaje);
            this.panel2.Controls.Add(this.btnSalas);
            this.panel2.Controls.Add(this.btnPaises);
            this.panel2.Controls.Add(this.btnHotel);
            this.panel2.Controls.Add(this.btnMovimientos);
            this.panel2.Controls.Add(this.btnPartidas);
            this.panel2.Controls.Add(this.btnArbitros);
            this.panel2.Controls.Add(this.btnJugadores);
            this.panel2.Controls.Add(this.btnParticipantes);
            this.panel2.Location = new System.Drawing.Point(12, 131);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(147, 575);
            this.panel2.TabIndex = 1;
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.Location = new System.Drawing.Point(176, 78);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(780, 650);
            this.PanelContenedor.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Agency FB", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 44);
            this.label1.TabIndex = 3;
            this.label1.Text = "CLUB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Agency FB", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 44);
            this.label2.TabIndex = 4;
            this.label2.Text = "AJEDREZ";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(855, 47);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(40, 20);
            this.lblUser.TabIndex = 5;
            this.lblUser.Text = "label3";
            // 
            // iconPictureBox1
            // 
            this.iconPictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.iconPictureBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox1.IconChar = FontAwesome.Sharp.IconChar.User;
            this.iconPictureBox1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iconPictureBox1.Location = new System.Drawing.Point(817, 40);
            this.iconPictureBox1.Name = "iconPictureBox1";
            this.iconPictureBox1.Size = new System.Drawing.Size(32, 32);
            this.iconPictureBox1.TabIndex = 6;
            this.iconPictureBox1.TabStop = false;
            // 
            // iconButton1
            // 
            this.iconButton1.BackColor = System.Drawing.Color.Black;
            this.iconButton1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton1.Font = new System.Drawing.Font("Agency FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton1.ForeColor = System.Drawing.Color.White;
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.User;
            this.iconButton1.IconColor = System.Drawing.Color.Red;
            this.iconButton1.IconSize = 16;
            this.iconButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconButton1.Location = new System.Drawing.Point(17, 521);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Rotation = 0D;
            this.iconButton1.Size = new System.Drawing.Size(115, 33);
            this.iconButton1.TabIndex = 10;
            this.iconButton1.Text = "ADMIN";
            this.iconButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.iconButton1.UseVisualStyleBackColor = false;
            this.iconButton1.Click += new System.EventHandler(this.IconButton1_Click_1);
            // 
            // btnHospedaje
            // 
            this.btnHospedaje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnHospedaje.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnHospedaje.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHospedaje.ForeColor = System.Drawing.Color.Black;
            this.btnHospedaje.IconChar = FontAwesome.Sharp.IconChar.Clipboard;
            this.btnHospedaje.IconColor = System.Drawing.Color.Black;
            this.btnHospedaje.IconSize = 25;
            this.btnHospedaje.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHospedaje.Location = new System.Drawing.Point(0, 248);
            this.btnHospedaje.Name = "btnHospedaje";
            this.btnHospedaje.Rotation = 0D;
            this.btnHospedaje.Size = new System.Drawing.Size(147, 41);
            this.btnHospedaje.TabIndex = 6;
            this.btnHospedaje.Text = "Hospedaje";
            this.btnHospedaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHospedaje.UseVisualStyleBackColor = false;
            this.btnHospedaje.Click += new System.EventHandler(this.BtnHospedaje_Click);
            // 
            // btnSalas
            // 
            this.btnSalas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSalas.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnSalas.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalas.IconChar = FontAwesome.Sharp.IconChar.Sellcast;
            this.btnSalas.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnSalas.IconSize = 25;
            this.btnSalas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalas.Location = new System.Drawing.Point(0, 201);
            this.btnSalas.Name = "btnSalas";
            this.btnSalas.Rotation = 0D;
            this.btnSalas.Size = new System.Drawing.Size(147, 41);
            this.btnSalas.TabIndex = 8;
            this.btnSalas.Text = "Salas";
            this.btnSalas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSalas.UseVisualStyleBackColor = false;
            this.btnSalas.Click += new System.EventHandler(this.BtnSalas_Click);
            // 
            // btnPaises
            // 
            this.btnPaises.BackColor = System.Drawing.Color.Orange;
            this.btnPaises.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnPaises.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaises.IconChar = FontAwesome.Sharp.IconChar.FlagUsa;
            this.btnPaises.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnPaises.IconSize = 25;
            this.btnPaises.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPaises.Location = new System.Drawing.Point(0, 403);
            this.btnPaises.Name = "btnPaises";
            this.btnPaises.Rotation = 0D;
            this.btnPaises.Size = new System.Drawing.Size(147, 41);
            this.btnPaises.TabIndex = 9;
            this.btnPaises.Text = "Paises";
            this.btnPaises.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPaises.UseVisualStyleBackColor = false;
            this.btnPaises.Click += new System.EventHandler(this.BtnPaises_Click);
            // 
            // btnHotel
            // 
            this.btnHotel.BackColor = System.Drawing.Color.Lime;
            this.btnHotel.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnHotel.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHotel.ForeColor = System.Drawing.Color.White;
            this.btnHotel.IconChar = FontAwesome.Sharp.IconChar.Hotel;
            this.btnHotel.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnHotel.IconSize = 25;
            this.btnHotel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHotel.Location = new System.Drawing.Point(0, 154);
            this.btnHotel.Name = "btnHotel";
            this.btnHotel.Rotation = 0D;
            this.btnHotel.Size = new System.Drawing.Size(147, 41);
            this.btnHotel.TabIndex = 7;
            this.btnHotel.Text = "Hotel";
            this.btnHotel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHotel.UseVisualStyleBackColor = false;
            this.btnHotel.Click += new System.EventHandler(this.BtnHotel_Click);
            // 
            // btnMovimientos
            // 
            this.btnMovimientos.BackColor = System.Drawing.Color.Plum;
            this.btnMovimientos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnMovimientos.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMovimientos.IconChar = FontAwesome.Sharp.IconChar.DiceOne;
            this.btnMovimientos.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnMovimientos.IconSize = 25;
            this.btnMovimientos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMovimientos.Location = new System.Drawing.Point(0, 356);
            this.btnMovimientos.Name = "btnMovimientos";
            this.btnMovimientos.Rotation = 0D;
            this.btnMovimientos.Size = new System.Drawing.Size(147, 41);
            this.btnMovimientos.TabIndex = 5;
            this.btnMovimientos.Text = "Movimientos";
            this.btnMovimientos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMovimientos.UseVisualStyleBackColor = false;
            this.btnMovimientos.Click += new System.EventHandler(this.BtnMovimientos_Click);
            // 
            // btnPartidas
            // 
            this.btnPartidas.BackColor = System.Drawing.Color.Purple;
            this.btnPartidas.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnPartidas.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartidas.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPartidas.IconChar = FontAwesome.Sharp.IconChar.Chess;
            this.btnPartidas.IconColor = System.Drawing.Color.White;
            this.btnPartidas.IconSize = 25;
            this.btnPartidas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPartidas.Location = new System.Drawing.Point(0, 309);
            this.btnPartidas.Name = "btnPartidas";
            this.btnPartidas.Rotation = 0D;
            this.btnPartidas.Size = new System.Drawing.Size(147, 41);
            this.btnPartidas.TabIndex = 3;
            this.btnPartidas.Text = "Partidas";
            this.btnPartidas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPartidas.UseVisualStyleBackColor = false;
            this.btnPartidas.Click += new System.EventHandler(this.BtnPartidas_Click);
            // 
            // btnArbitros
            // 
            this.btnArbitros.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnArbitros.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnArbitros.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnArbitros.IconChar = FontAwesome.Sharp.IconChar.UserCircle;
            this.btnArbitros.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btnArbitros.IconSize = 25;
            this.btnArbitros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnArbitros.Location = new System.Drawing.Point(0, 94);
            this.btnArbitros.Name = "btnArbitros";
            this.btnArbitros.Rotation = 0D;
            this.btnArbitros.Size = new System.Drawing.Size(147, 41);
            this.btnArbitros.TabIndex = 2;
            this.btnArbitros.Text = "Arbitros";
            this.btnArbitros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnArbitros.UseVisualStyleBackColor = false;
            this.btnArbitros.Click += new System.EventHandler(this.BtnArbitros_Click);
            // 
            // btnJugadores
            // 
            this.btnJugadores.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnJugadores.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnJugadores.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJugadores.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnJugadores.IconChar = FontAwesome.Sharp.IconChar.Xbox;
            this.btnJugadores.IconColor = System.Drawing.Color.Black;
            this.btnJugadores.IconSize = 25;
            this.btnJugadores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJugadores.Location = new System.Drawing.Point(0, 47);
            this.btnJugadores.Name = "btnJugadores";
            this.btnJugadores.Rotation = 0D;
            this.btnJugadores.Size = new System.Drawing.Size(147, 41);
            this.btnJugadores.TabIndex = 1;
            this.btnJugadores.Text = "Jugadores";
            this.btnJugadores.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnJugadores.UseVisualStyleBackColor = false;
            this.btnJugadores.Click += new System.EventHandler(this.BtnJugadores_Click);
            // 
            // btnParticipantes
            // 
            this.btnParticipantes.BackColor = System.Drawing.Color.MediumBlue;
            this.btnParticipantes.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btnParticipantes.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParticipantes.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnParticipantes.IconChar = FontAwesome.Sharp.IconChar.UserAlt;
            this.btnParticipantes.IconColor = System.Drawing.Color.WhiteSmoke;
            this.btnParticipantes.IconSize = 25;
            this.btnParticipantes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParticipantes.Location = new System.Drawing.Point(0, 0);
            this.btnParticipantes.Name = "btnParticipantes";
            this.btnParticipantes.Rotation = 0D;
            this.btnParticipantes.Size = new System.Drawing.Size(147, 41);
            this.btnParticipantes.TabIndex = 0;
            this.btnParticipantes.Text = "Participantes";
            this.btnParticipantes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnParticipantes.UseVisualStyleBackColor = false;
            this.btnParticipantes.Click += new System.EventHandler(this.IconButton1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(977, 35);
            this.panel1.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Red;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(943, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 35);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 740);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.iconPictureBox1);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PanelContenedor);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private FontAwesome.Sharp.IconButton btnPaises;
        private FontAwesome.Sharp.IconButton btnSalas;
        private FontAwesome.Sharp.IconButton btnHotel;
        private FontAwesome.Sharp.IconButton btnHospedaje;
        private FontAwesome.Sharp.IconButton btnMovimientos;
        private FontAwesome.Sharp.IconButton btnPartidas;
        private FontAwesome.Sharp.IconButton btnArbitros;
        private FontAwesome.Sharp.IconButton btnJugadores;
        private FontAwesome.Sharp.IconButton btnParticipantes;
        private System.Windows.Forms.Panel PanelContenedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private FontAwesome.Sharp.IconButton iconButton1;
        private FontAwesome.Sharp.IconPictureBox iconPictureBox1;
        public System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
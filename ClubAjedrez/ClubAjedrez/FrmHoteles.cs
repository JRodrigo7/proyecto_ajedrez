﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;
namespace ClubAjedrez
{
    public partial class FrmHoteles : Form
    {
        private HotelesManejador _HotelesManejador;
        public FrmHoteles()
        {
            InitializeComponent();
            _HotelesManejador = new HotelesManejador();

            
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            lblid.Enabled = activar;
            txtnombre.Enabled = activar;
            txtdireccion.Enabled = activar;
            txttelefono.Enabled = activar;
            

        }
        private void LimpiarCuadros()
        {
            lblid.Text = "0";
            txtnombre.Text = "";
            txttelefono.Text = "";
            txtdireccion.Text = "";
            
        }

        private void FrmHoteles_Load(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            Buscarhoteles("");


        }
        private void Buscarhoteles(string filtro)
        {
            dtgHotel.DataSource = _HotelesManejador.GetHoteles(filtro);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtnombre.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarHotel();
                LimpiarCuadros();
                Buscarhoteles("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarHotel()
        {
            _HotelesManejador.Guardar(new Hoteles
            {
                Idhotel = Convert.ToInt32(lblid.Text),
                Nombre = txtnombre.Text,
                Direccion = txtdireccion.Text,
                Telefono = txttelefono.Text,
               

            }); ;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            Buscarhoteles(txtbuscar.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este jugador?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarHotel();
                    Buscarhoteles("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarHotel()
        {
            var idhotel = dtgHotel.CurrentRow.Cells["idhotel"].Value;
            _HotelesManejador.Eliminar(Convert.ToInt32(idhotel));
        }

        private void dtgHotel_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarHoteles();
                Buscarhoteles("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ModificarHoteles()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblid.Text = dtgHotel.CurrentRow.Cells["Idhotel"].Value.ToString();
            txtnombre.Text = dtgHotel.CurrentRow.Cells["Nombre"].Value.ToString();
            txtdireccion.Text = dtgHotel.CurrentRow.Cells["Direccion"].Value.ToString();
            txttelefono.Text = dtgHotel.CurrentRow.Cells["Telefono"].Value.ToString();
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtdireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txttelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmMovimientos : Form
    {
        private MovimientoManejador _movimientoManejador;
        private PartidaManejador _partidaManejador;
        public FrmMovimientos()
        {
            InitializeComponent();
            _movimientoManejador = new MovimientoManejador();
            _partidaManejador = new PartidaManejador();
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            BuscarMovimiento("");
            BuscarPartida("");
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbPartida.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarMovimientos();
                LimpiarCuadros();
                BuscarMovimiento("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Estás seguro que deseas eliminar este movimiento?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMovimmiento();
                    BuscarMovimiento("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            txtBuscar.Text = "";
            txtComentario.Text = "";
            txtNoMovimiento.Text = "";
            txtPosiciones.Text = "";
            cmbPartida.Text = "";
        }
        private void ControlarCuadros(bool activar)
        {
            lblId.Enabled = activar;
            txtBuscar.Enabled = activar;
            txtComentario.Enabled = activar;
            txtNoMovimiento.Enabled = activar;
            txtPosiciones.Enabled = activar;
            cmbPartida.Enabled = activar;
        }
        private void BuscarMovimiento(string filtro)
        {
            dgvMovimientos.DataSource = _movimientoManejador.GetMovimientos(filtro);
        }
        private void GuardarMovimientos()
        {
            _movimientoManejador.Guardar(new Movimiento
            {
                IdMovimiento = Convert.ToInt32(lblId.Text),
                NumMovimiento = Convert.ToInt32(txtNoMovimiento.Text),
                Posiciones = txtPosiciones.Text,
                Descripcion = txtComentario.Text,
                fkPartida = Convert.ToInt32(cmbPartida.Text),
            }); ;
        }
        private void EliminarMovimmiento()
        {
            var idmovimiento = dgvMovimientos.CurrentRow.Cells["idmovimiento"].Value;
            _movimientoManejador.Eliminar(Convert.ToInt32(idmovimiento));
        }

        private void dgvMovimientos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMovimientos();
                BuscarMovimiento("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarMovimientos()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblId.Text = dgvMovimientos.CurrentRow.Cells["idmovimiento"].Value.ToString();
            txtNoMovimiento.Text = dgvMovimientos.CurrentRow.Cells["nummovimiento"].Value.ToString();
            txtPosiciones.Text = dgvMovimientos.CurrentRow.Cells["posiciones"].Value.ToString();
            txtComentario.Text = dgvMovimientos.CurrentRow.Cells["descripcion"].Value.ToString();
            cmbPartida.Text = dgvMovimientos.CurrentRow.Cells["fkpartida"].Value.ToString();
        }
        private void BuscarPartida(string filtro)
        {
            cmbPartida.DataSource = _partidaManejador.GetPartidas(filtro);
            cmbPartida.DisplayMember = "idpartida";
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtNoMovimiento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtPosiciones_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtComentario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }
    }
}

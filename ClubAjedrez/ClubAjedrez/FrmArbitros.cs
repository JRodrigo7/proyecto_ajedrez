﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmArbitros : Form
    {
        private ArbitrosManejador _arbitroManejador;
        private PaisesManejador _paisesManejador;
        public FrmArbitros()
        {
            InitializeComponent();
            _arbitroManejador = new ArbitrosManejador();
            _paisesManejador = new PaisesManejador();
        }

        private void FrmArbitros_Load(object sender, EventArgs e)
        {
            BuscarArbitros("");
            MostrarPais("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void MostrarPais(string filtro)
        {
            cmbPais.DataSource = _paisesManejador.GetPaises2(filtro);

            cmbPais.ValueMember = "idpais";
            cmbPais.DisplayMember = "nombrepais";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApp.Enabled = activar;
            txtApm.Enabled = activar;
            txtTelefono.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtNoAsociado.Enabled = activar;
            cmbPais.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            txtNombre.Text = "";
            txtApp.Text = "";
            txtApm.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";
            txtNoAsociado.Text = "";
            cmbPais.Text = "";
        }
        private void BuscarArbitros(string filtro)
        {
            dgvArbitros.DataSource = _arbitroManejador.GetArbitros(filtro);
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }
        private void GuardarArbitro()
        {
            _arbitroManejador.Guardar(new Arbitros
            {
                Idarbitro = Convert.ToInt32(lblId.Text),
                Numsocio = txtNoAsociado.Text,
                Nombre = txtNombre.Text,
                Apellidopaterno = txtApp.Text,
                Apellidomaterno = txtApm.Text,
                Direcccion = txtDireccion.Text,
                Telefono = txtTelefono.Text,
                Pais = cmbPais.SelectedIndex

            }); ;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarArbitro();
                LimpiarCuadros();
                BuscarArbitros("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarArbitros(txtBuscar.Text);
        }
        private void EliminarArbitro()
        {
            var idarbitro = dgvArbitros.CurrentRow.Cells["idarbitro"].Value;
            _arbitroManejador.Eliminar(Convert.ToInt32(idarbitro));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este arbitro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarArbitro();
                    BuscarArbitros("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void ModificarArbitro()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);




            lblId.Text = dgvArbitros.CurrentRow.Cells["idarbitro"].Value.ToString();
            txtNoAsociado.Text = dgvArbitros.CurrentRow.Cells["Numsocio"].Value.ToString();
            txtNombre.Text = dgvArbitros.CurrentRow.Cells["nombre"].Value.ToString();
            txtApp.Text = dgvArbitros.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtApm.Text = dgvArbitros.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtDireccion.Text = dgvArbitros.CurrentRow.Cells["Direcccion"].Value.ToString();
            txtTelefono.Text = dgvArbitros.CurrentRow.Cells["telefono"].Value.ToString();
            cmbPais.Text = dgvArbitros.CurrentRow.Cells["pais"].Value.ToString();

        }

        private void DgvArbitros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarArbitro();
                BuscarArbitros("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtNoAsociado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if((e.KeyChar >= 32 && e.KeyChar <=47) || (e.KeyChar >= 58 && e.KeyChar <=255))
            {
                e.Handled = true;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar)&&(e.KeyChar !=(char)Keys.Back)&& (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtApp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtApm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmJugadores : Form
    {
        private JugadoresManejador _jugadorManejador;
        private PaisesManejador _paisesManejador;
        public FrmJugadores()
        {
            InitializeComponent();
            _jugadorManejador = new JugadoresManejador();
            _paisesManejador = new PaisesManejador();
        }

        private void FrmJugadores_Load(object sender, EventArgs e)
        {
            BuscarJugadores("");
            MostrarPais("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void MostrarPais(string filtro)
        {
            cmbPais.DataSource = _paisesManejador.GetPaises2(filtro);

            cmbPais.ValueMember = "idpais";
            cmbPais.DisplayMember = "nombrepais";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApp.Enabled = activar;
            txtApm.Enabled = activar;
            txtTelefono.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtNivel.Enabled = activar;
            txtNoAsociado.Enabled = activar;
            cmbPais.Enabled = activar;
            

        }
        private void LimpiarCuadros()
        {
            lblId.Text = "0";
            txtNombre.Text = "";
            txtApp.Text = "";
            txtApm.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";
            txtNivel.Text = "";
            txtNoAsociado.Text = "";
            cmbPais.Text = "";
        }
        private void BuscarJugadores(string filtro)
        {
            dgvJugadores.DataSource = _jugadorManejador.getJugadores(filtro);
        }

        private void BtnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }
        private void GuardarJugador()
        {
            _jugadorManejador.Guardar(new Jugadores
            {
                Idjugador = Convert.ToInt32(lblId.Text),
                Numsocio = txtNoAsociado.Text,
                Nombre = txtNombre.Text,
                Apellidopaterno = txtApp.Text,
                Apellidomaterno = txtApm.Text,
                Direcccion = txtDireccion.Text,
                Telefono = txtTelefono.Text,
                Pais = Convert.ToInt32(cmbPais.SelectedValue),
                Nivel = Convert.ToInt32(txtNivel.Text)

            }); ;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarJugador();
                LimpiarCuadros();
                BuscarJugadores("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarJugadores(txtBuscar.Text);
        }
        private void EliminarJugador()
        {
            var idjugador = dgvJugadores.CurrentRow.Cells["idjugador"].Value;
            _jugadorManejador.Eliminar(Convert.ToInt32(idjugador));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este jugador?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarJugador();
                    BuscarJugadores("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void DgvJugadores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void ModificarJugador()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

           


            lblId.Text = dgvJugadores.CurrentRow.Cells["idjugador"].Value.ToString();
            txtNoAsociado.Text = dgvJugadores.CurrentRow.Cells["Numsocio"].Value.ToString();
            txtNombre.Text = dgvJugadores.CurrentRow.Cells["nombre"].Value.ToString();
            txtApp.Text = dgvJugadores.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            txtApm.Text = dgvJugadores.CurrentRow.Cells["apellidomaterno"].Value.ToString();
            txtDireccion.Text = dgvJugadores.CurrentRow.Cells["Direcccion"].Value.ToString();
            txtTelefono.Text = dgvJugadores.CurrentRow.Cells["telefono"].Value.ToString();
            txtNivel.Text = dgvJugadores.CurrentRow.Cells["nivel"].Value.ToString();
            cmbPais.Text = dgvJugadores.CurrentRow.Cells["pais"].Value.ToString();

        }

        private void DgvJugadores_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarJugador();
                BuscarJugadores("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtNoAsociado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtNivel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtApp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtApm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }
    }
}

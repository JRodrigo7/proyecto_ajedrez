﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;

namespace ClubAjedrez
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void IconButton1_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmParticipantes());
        }

        private void BtnJugadores_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmJugadores());
        }
        private Form activeForm = null;
        private void AbrirFormularioHijo(Form frmhijo)
        {
            if (activeForm != null)

                activeForm.Close();
            activeForm = frmhijo;
            frmhijo.TopLevel = false;
            frmhijo.Dock = DockStyle.Fill;
            PanelContenedor.Controls.Add(frmhijo);
            PanelContenedor.Tag = frmhijo;
            frmhijo.Show();

        }

        private void BtnArbitros_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmArbitros());
        }

        private void BtnHotel_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmHoteles());
        }

        private void BtnSalas_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmSalas());
        }

        private void BtnHospedaje_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmHospedaje());
        }

        private void BtnPartidas_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmPartida());
        }

        private void BtnMovimientos_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmMovimientos());
        }

        private void BtnPaises_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmPaises());
        }

        private void IconButton1_Click_1(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new FrmAdmin());
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;
using System.Windows.Forms;

namespace ClubAjedrez
{
    public partial class FrmSalas : Form
    {
        private HotelesManejador _HotelesManejador;
        private SalasManejador salasManejador;
        public FrmSalas()
        {
            InitializeComponent();
            _HotelesManejador = new HotelesManejador();
            salasManejador = new SalasManejador();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            lblid.Enabled = activar;
            txtNosala.Enabled = activar;
            txtCapacidad.Enabled = activar;
            txtMedios.Enabled = activar;
            cmbHotel.Enabled = activar;


        }
        private void LimpiarCuadros()
        {
            lblid.Text = "0";
            txtNosala.Text = "";
            txtCapacidad.Text = "";
            txtMedios.Text = "";
            cmbHotel.Text = "";

        }
        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void FrmSalas_Load(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            BuscarSalas("");
            codigo("");
            //_HotelesManejador.gethotelescm(cmbHotel);
        }

        private void BuscarSalas(string filtro)
        {
            dgvsalas.DataSource = salasManejador.GetSalas(filtro);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNosala.Focus();
        }
       
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                Guardarsalas();
                LimpiarCuadros();
                BuscarSalas("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Guardarsalas()
        {
            salasManejador.Guardar(new Salas
            {
                Ids = Convert.ToInt32(lblid.Text),
                Numsala = Convert.ToInt32(txtNosala.Text),
                Cap = Convert.ToInt32(txtCapacidad.Text),
                Med = txtMedios.Text,
                Cod = Convert.ToInt32(cmbHotel.SelectedValue)


            }); 
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarSalas(txtBuscar.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este jugador?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarSalas();
                    BuscarSalas("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarSalas()
        {
            var idsala = dgvsalas.CurrentRow.Cells["idsala"].Value;
            salasManejador.Eliminar(Convert.ToInt32(idsala));
        }

        private void dgvsalas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarSalas();
                BuscarSalas("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ModificarSalas()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblid.Text = dgvsalas.CurrentRow.Cells["Idsala"].Value.ToString();
            txtNosala.Text = dgvsalas.CurrentRow.Cells["Numero_de_sala"].Value.ToString();
            txtCapacidad.Text = dgvsalas.CurrentRow.Cells["Capacidad"].Value.ToString();
            txtMedios.Text = dgvsalas.CurrentRow.Cells["Medios"].Value.ToString();
            cmbHotel.Text = dgvsalas.CurrentRow.Cells["Nombre_Hotel"].Value.ToString();
        }

        private void cmbHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Codigo("");
        }

        private void cmbHotel_Click(object sender, EventArgs e)
        {
            codigo("");
        }
        public void codigo(string filtro)
        {
            cmbHotel.DataSource = _HotelesManejador.GetHoteles(filtro);
            cmbHotel.DisplayMember = "nombre";
            cmbHotel.ValueMember = "idhotel";
            
        }

        private void txtNosala_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtCapacidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                e.Handled = true;
            }
        }

        private void txtMedios_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

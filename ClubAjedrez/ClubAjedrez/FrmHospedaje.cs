﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ClubAjedrez;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class FrmHospedaje : Form
    {
        HospedajeManejador hospedajeManejador;
        HotelesManejador HotelesManejador;
        ParticipantesManejador participatesManejador;
        public FrmHospedaje()
        {
            InitializeComponent();
            hospedajeManejador = new HospedajeManejador();
            HotelesManejador = new HotelesManejador();
            participatesManejador = new ParticipantesManejador();

           

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnInsertar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            lblid.Enabled = activar;
            cmbparticipante.Enabled = activar;
            cmbhotel.Enabled = activar;
            txtfechainicial.Enabled = activar;
            txtfechafinal.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            lblid.Text = "0";
            cmbparticipante.Text = "";
            cmbhotel.Text = "";
            txtfechainicial.Text = "";
            txtfechafinal.Text = "";

        }



        private void FrmHospedaje_Load(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            Buscarhospedaje(" ");
            Buscarhospedaje("");
            hospedajeManejador.gethoteles2(cmbhotel);
            //hospedajeManejador.GetParticipantes2(cmbparticipante);
            codigo2("");

        }
        
        private void Buscarhospedaje(string filtro)
        {
            dtghospedajes.DataSource = hospedajeManejador.GetHospedajes(filtro);
        }
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtfechainicial.Focus();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarHospedaje();
                LimpiarCuadros();
                Buscarhospedaje("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarHospedaje()
        {
            hospedajeManejador.Guardar(new Hospedajes
            {
                Idhospedaje = int.Parse(lblid.Text),
                Fkparticiapnte = Convert.ToInt32(cmbparticipante.SelectedValue),
                Fkhotel = int.Parse(cmbhotel.SelectedValue.ToString()),
                Fechainicial = txtfechainicial.Text,
                Fechafinal = txtfechafinal.Text,
                

            }); ;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este jugador?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarHospedaje();
                    Buscarhospedaje("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EliminarHospedaje()
        {
            var idhospedaje = dtghospedajes.CurrentRow.Cells["idhospedaje"].Value;
            hospedajeManejador.Eliminar(Convert.ToInt32(idhospedaje));
        }

        private void dtghospedajes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarHospedaje();
                Buscarhospedaje("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void ModificarHospedaje()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);


            lblid.Text = dtghospedajes.CurrentRow.Cells["Idhospedaje"].Value.ToString();
            cmbparticipante.Text = dtghospedajes.CurrentRow.Cells["Participante"].Value.ToString();
            cmbhotel.Text = dtghospedajes.CurrentRow.Cells["Nombre"].Value.ToString();
            txtfechainicial.Text = dtghospedajes.CurrentRow.Cells["Fechainicial"].Value.ToString();
            txtfechafinal.Text = dtghospedajes.CurrentRow.Cells["Fechafinal"].Value.ToString();

        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            Buscarhospedaje(txtbuscar.Text);
        }
        public void codigo(string filtro)
        {
            cmbhotel.DataSource = HotelesManejador.GetHoteles(filtro);
            cmbhotel.ValueMember = "idhotel";
            cmbhotel.DisplayMember = "nombre";
        }
        public void codigo2(string filtro)
        {
            cmbparticipante.DataSource = participatesManejador.GetParticipantes(filtro);
            cmbparticipante.ValueMember = "idparticipante";
            cmbparticipante.DisplayMember = "nombre";
        }

        private void cmbhotel_Click(object sender, EventArgs e)
        {
            codigo("");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Cmbparticipante_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtfechainicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtfechafinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtbuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
    }
}

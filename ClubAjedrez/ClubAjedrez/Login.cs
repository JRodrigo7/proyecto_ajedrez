﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ClubAjedrez;

namespace ClubAjedrez
{
    public partial class Login : Form
    {
        private AdminManejador _adminmanejador;

        string usuario = "";
        string pass = "";
        string user;
        string psw;
        public Login()
        {
            InitializeComponent();
            _adminmanejador = new AdminManejador();

        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUsuario.Text = "Usuario";
            txtUsuario.ForeColor = Color.Gray;
            txtPass.Text = "Contraseña";
            txtPass.ForeColor = Color.Gray;
        }

        private void TxtUsuario_Enter(object sender, EventArgs e)
        {
            txtUsuario.Text = "";
            txtUsuario.ForeColor = Color.Black;
        }

        private void TxtUsuario_Leave(object sender, EventArgs e)
        {
            usuario = txtUsuario.Text;
            if (usuario.Equals("Usuario"))
            {
                txtUsuario.Text = "Usuario";
                txtUsuario.ForeColor = Color.Gray;
            }
            else
            {
                if (usuario.Equals(""))
                {
                    txtUsuario.Text = "Usuario";
                    txtUsuario.ForeColor = Color.Gray;
                }
                else
                {
                    txtUsuario.Text = usuario;
                    txtUsuario.ForeColor = Color.Black;
                }
            }
        }

        private void TxtPass_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtPass_Enter(object sender, EventArgs e)
        {
            txtPass.Text = "";
            txtPass.ForeColor = Color.Black;
        }

        private void TxtPass_Leave(object sender, EventArgs e)
        {
            pass = txtPass.Text;
            if (pass.Equals("Contraseña"))
            {
                txtPass.Text = "Contraseña";
                txtPass.ForeColor = Color.Gray;
            }
            else
            {
                if (pass.Equals(""))
                {
                    txtPass.Text = "Contraseña";
                    txtPass.ForeColor = Color.Gray;
                }
                else
                {
                    txtPass.Text = pass;
                    txtPass.ForeColor = Color.Black;
                }
            }
        }

        private void BtnEntrar_Click(object sender, EventArgs e)
        {
           
            try
            {
                BuscarUsuario();
                BuscarPass();
                if (user == psw)
                {


                    Menu f = new Menu();
                    f.lblUser.Text = txtUsuario.Text;
                    f.Show();

                }
                else
                {
                    MessageBox.Show("Usuario o Contraseña incorrectos");
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Usuario o Contraseña incorrectos");

            }

        }

        private void BuscarUsuario()
        {
            var t = new DataSet();
            t = _adminmanejador.consultaUsuario(txtUsuario.Text);
            user = t.Tables[0].Rows[0]["id"].ToString();
        }
        private void BuscarPass()
        {
            var t = new DataSet();
            t = _adminmanejador.consultaPass(txtPass.Text);
            psw = t.Tables[0].Rows[0]["id"].ToString();
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar <= 32 && e.KeyChar >= 126)
            {
                e.Handled = true;
            }
        }
    }
}
